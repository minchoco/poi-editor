import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {
  Typography, Paper, Grid,
} from '@material-ui/core'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 20,
    color: theme.palette.text.secondary,
  },
  paper: {
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '30px',
  },
})

class ErrorBoundary extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hasError: false,
    }
  }

  componentDidCatch(error) {
    this.setState({ hasError: true })
    console.log(`Error: ${error}`)
  }

  render() {
    const { hasError } = this.state
    const { classes, children } = this.props
    return hasError
      ? (
        <Grid
          container={true} spacing={24} alignItems='center'
          className={classes.root}>
          <Paper className={classes.paper}>
            <Grid row={true}>
              <Grid item={true} xs={12}>
                <Typography>알 수 없는 문제가 발생했습니다. :( </Typography>
                <Typography>시스템 개선을 위해 오류 내용은 송민정 대리(minjeong.song@kt.com)에게 전달 부탁드립니다.</Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      ) : children
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.object,
  classes: PropTypes.object,
}

export default withStyles(styles)(ErrorBoundary)
