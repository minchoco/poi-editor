
export default class Category {
    code;

    name;

    constructor(code, name) {
      this.code = code
      this.name = name
    }
}
