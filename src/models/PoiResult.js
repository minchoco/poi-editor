export default class PoiResult {
    id;

    name;

    subName;

    branch;

    category;

    address;

    point;

    phones;

    distance;

    extension;

    children;

    // 변경을 위한 field
    originalName;

    originalBranch;

    originalCategory;

    originalAddress;

    originalPhones;

    newCategory;

    newAddress;

    constructor(result) {
      if (result !== undefined) {
        this.id = result.id
        this.name = result.name
        this.subName = result.subName
        this.branch = result.branch
        this.category = result.category
        this.address = result.address
        this.point = result.point
        this.phones = result.phones
        this.distance = result.distance
        this.extension = result.extension
        this.children = result.children
        this.originalName = result.name
        this.originalBranch = result.branch
        this.originalCategory = result.category
        this.originalAddress = result.address
        this.originalPhones = result.phones
      }
    }

    getId = () => this.id

    getName = () => {
      let title = this.name
      if (this.subName !== undefined && this.subName !== '') { title += ` ${this.subName}` }
      if (this.branch !== undefined && this.branch !== '') { title += ` (${this.branch})` }
      return title
    }

    getCategory = () => {
      if (this.category !== undefined) {
        return `${this.category.masterName} > ${this.category.middleName} > ${this.category.subName}`
      }
      return ''
    }

    validCategory = category => category !== undefined && !category.masterCode.includes('all') && !category.middleCode.includes('all') && !category.subCode.includes('all')

    setAddress = (address) => {
      if (address.parcelAddress !== undefined && address.parcelAddress.length !== 0) {
        this.address = {
          siDo: address.parcelAddress[0].siDo,
          siGunGu: address.parcelAddress[0].siGunGu,
          eupMyeonDong: (address.parcelAddress[0].eupMyeonDong !== undefined) ? address.parcelAddress[0].eupMyeonDong : '',
          haengJeongDong: (address.parcelAddress[0].haengJeongDong !== undefined) ? address.parcelAddress[0].haengJeongDong : '',
          ri: (address.parcelAddress[0].ri !== undefined) ? address.parcelAddress[0].ri : '',
          houseNumber: (address.parcelAddress[0].houseNumber !== undefined) ? address.parcelAddress[0].houseNumber : '',
        }
      }
      if (address.roadAddress !== undefined && address.roadAddress.length !== 0) {
        this.address = {
          ...this.address,
          ...{
            eupMyeon: (address.roadAddress[0].eupMyeon !== undefined) ? address.roadAddress[0].eupMyeon : '',
            street: (address.roadAddress[0].street !== undefined) ? address.roadAddress[0].street : '',
            streetNumber: (address.roadAddress[0].streetNumber !== undefined) ? address.roadAddress[0].streetNumber : '',
          },
        }
      } else {
        this.address = {
          ...this.address,
          ...{
            eupMyeon: '',
            street: '',
            streetNumber: '',
          },
        }
      }
    }

    isDetailParcelAddress = () => {
      if (this.newAddress.parcelAddress !== undefined
        && this.newAddress.parcelAddress.length !== 0) {
        return this.newAddress.parcelAddress[0].houseNumber !== undefined && this.newAddress.parcelAddress[0].houseNumber !== ''
      }
      return undefined
    }

    getRoadAddress = () => {
      if (this.address !== undefined) {
        let address = `${this.address.siDo} ${this.address.siGunGu}`
        if (this.address.eupMyeon !== undefined && this.address.eupMyeon !== '') {
          address += ` ${this.address.eupMyeon}`
        }
        if (this.address.street !== undefined && this.address.street !== '') {
          address += ` ${this.address.street}`
        }
        if (this.address.streetNumber !== undefined && this.address.streetNumber !== '') {
          address += ` ${this.address.streetNumber}`
        }
        return address
      }
      return ''
    }

    getParcelAddress = () => {
      if (this.address !== undefined) {
        let address = `${this.address.siDo} ${this.address.siGunGu} ${this.address.eupMyeonDong}`
        if (this.address.ri !== undefined && this.address.ri !== '') {
          address += ` ${this.address.ri}`
        }
        return `${address} ${this.address.houseNumber}`
      }
      return ''
    }

    getSubParcelAddress = () => {
      let address = `${this.address.eupMyeonDong}`
      if (this.address.ri !== undefined && this.address.ri !== '') {
        address += ` ${this.address.ri}`
      }
      return `${address} ${this.address.houseNumber}`
    }

    getDistance = () => this.distance !== undefined && `| ${this.distance.toFixed(2)}m`

    setPhones = (key, value) => {
      if (this.phones === undefined) {
        this.phones = {}
      } else {
        this.phones[key] = value
      }
    }

    getRepresentationPhone = () => {
      let phone
      if (this.phones !== undefined) {
        if (this.phones.representation !== undefined) {
          if (Array.isArray(this.phones.representation)) {
            phone = this.phones.representation.join(', ')
          } else {
            phone = this.phones.representation
          }
        }
      }
      return phone
    }

    getNormalPhone = () => {
      let phone
      if (this.phones !== undefined) {
        if (this.phones.normal !== undefined) {
          if (Array.isArray(this.phones.normal)) {
            phone = this.phones.normal.join(', ')
          } else {
            phone = this.phones.normal
          }
        }
      }
      return phone
    }

    getFax = () => {
      let phone
      if (this.phones !== undefined) {
        if (this.phones.fax !== undefined) {
          if (Array.isArray(this.phones.fax)) {
            phone = this.phones.fax.join(', ')
          } else {
            phone = this.phones.fax
          }
        }
      }
      return phone
    }

    getDescription = () => {
      if (this.extension !== undefined) {
        if (this.extension.description !== undefined) {
          return this.extension.description
        }
      }
      return undefined
    }

    getHomePageURL = () => {
      if (this.extension !== undefined) {
        if (this.extension.homepageURL !== undefined) {
          return this.extension.homepageURL
        }
      }
      return undefined
    }

    getCreditcardService = () => {
      if (this.extension !== undefined) {
        if (this.extension.creditcardService !== undefined) {
          return this.extension.creditcardService
        }
      }
      return undefined
    }

    getHours24 = () => {
      if (this.extension !== undefined) {
        if (this.extension.hours24 !== undefined) {
          return this.extension.hours24
        }
      }
      return undefined
    }

    getParkingPrice = () => {
      if (this.extension !== undefined) {
        if (this.extension.parkingPrice !== undefined) {
          return this.extension.parkingPrice
        }
      }
      return undefined
    }

    getParkingService = () => {
      if (this.extension !== undefined) {
        if (this.extension.parkingService !== undefined) {
          return this.extension.parkingService
        }
      }
      return undefined
    }

    getParkingType = () => {
      if (this.extension !== undefined) {
        if (this.extension.parkingType !== undefined) {
          return this.extension.parkingType
        }
      }
      return undefined
    }

    getPetService = () => {
      if (this.extension !== undefined) {
        if (this.extension.petService !== undefined) {
          return this.extension.petService
        }
      }
      return undefined
    }

    getStrollerRental = () => {
      if (this.extension !== undefined) {
        if (this.extension.strollerRental !== undefined) {
          return this.extension.strollerRental
        }
      }
      return undefined
    }

    getOpeningHours = () => {
      if (this.extension !== undefined) {
        if (this.extension.openingHours !== undefined) {
          let info = ''
          if (this.extension.openingHours[1] !== undefined) {
            info += `월: ${this.extension.openingHours[1].start} ~ ${this.extension.openingHours[1].end}`
          }
          if (this.extension.openingHours[2] !== undefined) {
            info += ` 화: ${this.extension.openingHours[2].start} ~ ${this.extension.openingHours[2].end}`
          }
          if (this.extension.openingHours[3] !== undefined) {
            info += ` 수: ${this.extension.openingHours[3].start} ~ ${this.extension.openingHours[3].end}`
          }
          if (this.extension.openingHours[4] !== undefined) {
            info += ` 목: ${this.extension.openingHours[4].start} ~ ${this.extension.openingHours[4].end}`
          }
          if (this.extension.openingHours[5] !== undefined) {
            info += ` 금: ${this.extension.openingHours[5].start} ~ ${this.extension.openingHours[5].end}`
          }
          if (this.extension.openingHours[6] !== undefined) {
            info += ` 토: ${this.extension.openingHours[6].start} ~ ${this.extension.openingHours[6].end}`
          }
          if (this.extension.openingHours[0] !== undefined) {
            info += ` 일: ${this.extension.openingHours[0].start} ~ ${this.extension.openingHours[0].end}`
          }
          return info
        }
      }
      return undefined
    }
}
