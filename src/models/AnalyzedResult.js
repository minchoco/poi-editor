
export default class AnalyzedResult {
  poiTermType;

  poiTokens = '';

  addressTermType;

  addressTokens = '';

  constructor(evaluation) {
    if (evaluation.poiSearchBuilder !== undefined) {
      const builder = evaluation.poiSearchBuilder
      this.poiTermType = builder.termType
      if (builder.terms !== undefined && builder.terms !== null
        && builder.terms.struct !== undefined
        && builder.terms.struct.tokenInfo !== undefined
        && builder.terms.struct.tokenInfo.tokens !== undefined) {
        builder.terms.struct.tokenInfo.tokens.forEach((token) => {
          this.poiTokens += ` ${token.value} (${token.type})`
        })
      }
    }
    if (evaluation.addressBuilder !== undefined) {
      const builder = evaluation.addressBuilder
      this.addressTermType = builder.termType
      if (builder.terms !== undefined && builder.terms !== null
        && builder.terms.struct !== undefined
        && builder.terms.struct.tokenInfo !== undefined
        && builder.terms.struct.tokenInfo.tokens !== undefined) {
        builder.terms.struct.tokenInfo.tokens.forEach((token) => {
          this.addressTokens += ` ${token.value} (${token.type})`
        })
      }
    }
  }

  getPoiTermType = () => this.poiTermType

  getPoiTokens = () => this.poiTokens

  getAddressTermType = () => this.addressTermType

  getAddressTokens = () => this.addressTokens
}
