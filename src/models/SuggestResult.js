export default class SuggestResult {
    terms;

    parcelAddressList = [];

    constructor(terms, parcelAddressList) {
      this.terms = terms
      this.parcelAddressList = parcelAddressList
    }
}
