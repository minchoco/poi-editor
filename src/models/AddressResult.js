export default class AddressResult {
    id;

    parcelAddress;

    roadAddress;

    siDo;

    siGunGu;

    eupMyeonDong;

    ri;

    houseNumber;

    street;

    streetNumber;

    lat;

    lng;

    constructor(index, result) {
      this.id = index
      this.parcelAddress = result.parcelAddress
      this.roadAddress = result.roadAddress
      this.getShortAddress()
    }

    getCode = () => {
      let code = ''
      if (this.parcelAddress.length !== 0 && this.roadAddress.length !== 0) {
        this.parcelAddress.forEach((parcel) => {
          code = parcel.badmId
        })
      } else if (this.parcelAddress.length !== 0 && this.roadAddress.length === 0) {
        // this has only parcelAddress
        this.parcelAddress.forEach((parcel) => {
          code = parcel.badmId
        })
      } else if (this.parcelAddress.length === 0 && this.roadAddress.length !== 0) {
        // this has only roadAddress
        this.roadAddress.forEach((road) => {
          code = road.streetId
        })
      }
      console.log('getCode', code)
      return code
    }

    getAddress = () => {
      let fullAddress = ''
      if (this.parcelAddress.length !== 0 && this.roadAddress.length !== 0) {
        this.roadAddress.forEach((road) => {
          fullAddress += road.fullAddress
        })
        this.parcelAddress.forEach((parcel) => {
          fullAddress += `(${parcel.eupMyeonDong}`
          if (parcel.ri !== undefined) {
            fullAddress += ` ${parcel.ri}`
          }
          fullAddress += ` ${parcel.houseNumber})`
        })
      } else if (this.parcelAddress.length !== 0 && this.roadAddress.length === 0) {
        // this has only parcelAddress
        this.parcelAddress.forEach((parcel) => {
          fullAddress += parcel.fullAddress
        })
      } else if (this.parcelAddress.length === 0 && this.roadAddress.length !== 0) {
        // this has only roadAddress
        this.roadAddress.forEach((road) => {
          fullAddress += road.fullAddress
        })
      }
      return fullAddress
    }

    getShortAddress = () => {
      if (this.parcelAddress.length !== 0 && this.roadAddress.length !== 0) {
        this.siDo = this.roadAddress[0].siDo !== undefined ? this.roadAddress[0].siDo : ''
        this.siGunGu = this.roadAddress[0].siGunGu !== undefined ? this.roadAddress[0].siGunGu : ''
        this.eupMyeonDong = this.parcelAddress[0].eupMyeonDong !== undefined ? this.parcelAddress[0].eupMyeonDong : ''
        this.ri = this.parcelAddress[0].ri !== undefined ? this.parcelAddress[0].ri : ''
        this.street = this.roadAddress[0].street !== undefined ? this.roadAddress[0].street : ''
        this.houseNumber = this.parcelAddress[0].houseNumber !== undefined ? this.parcelAddress[0].houseNumber : ''
        this.streetNumber = this.roadAddress[0].streetNumber !== undefined ? this.roadAddress[0].streetNumber : ''

        if (this.parcelAddress[0].geographicInformation !== undefined
          && this.parcelAddress[0].geographicInformation.point !== undefined) {
          this.lat = this.parcelAddress[0].geographicInformation.point.lat
          this.lng = this.parcelAddress[0].geographicInformation.point.lng
        }
      } else if (this.parcelAddress.length !== 0 && this.roadAddress.length === 0) {
        this.siDo = this.parcelAddress[0].siDo !== undefined ? this.parcelAddress[0].siDo : ''
        this.siGunGu = this.parcelAddress[0].siGunGu !== undefined ? this.parcelAddress[0].siGunGu : ''
        this.eupMyeonDong = this.parcelAddress[0].eupMyeonDong !== undefined ? this.parcelAddress[0].eupMyeonDong : ''
        this.ri = this.parcelAddress[0].ri !== undefined ? this.parcelAddress[0].ri : ''
        this.houseNumber = this.parcelAddress[0].houseNumber !== undefined ? this.parcelAddress[0].houseNumber : ''
        if (this.parcelAddress[0].geographicInformation !== undefined
          && this.parcelAddress[0].geographicInformation.point !== undefined) {
          this.lat = this.parcelAddress[0].geographicInformation.point.lat
          this.lng = this.parcelAddress[0].geographicInformation.point.lng
        }
      } else if (this.parcelAddress.length === 0 && this.roadAddress.length !== 0) {
        this.siDo = this.roadAddress[0].siDo !== undefined ? this.roadAddress[0].siDo : ''
        this.siGunGu = this.roadAddress[0].siGunGu !== undefined ? this.roadAddress[0].siGunGu : ''
        this.street = this.roadAddress[0].street !== undefined ? this.roadAddress[0].street : ''
        this.streetNumber = this.roadAddress[0].streetNumber !== undefined ? this.roadAddress[0].streetNumber : ''
        if (this.roadAddress[0].geographicInformation !== undefined
          && this.roadAddress[0].geographicInformation.point !== undefined) {
          this.lat = this.roadAddress[0].geographicInformation.point.lat
          this.lng = this.roadAddress[0].geographicInformation.point.lng
        }
      }

      let address = this.siDo
      if (this.siGunGu !== '' && this.siGunGu !== undefined) {
        address += ` ${this.siGunGu}`
      }
      if (this.street !== '' && this.street !== undefined && this.eupMyeonDong !== '') {
        address += ` ${this.street} (${this.eupMyeonDong})`
      } else if (this.street !== '' && this.street !== undefined) {
        address += ` ${this.street}`
      } else if (this.eupMyeonDong !== '' && this.eupMyeonDong !== undefined) {
        address += ` ${this.eupMyeonDong}`
      }
      return address
    }
}
