/* eslint-disable react/jsx-filename-extension */
import React from 'react'
import { observable, action } from 'mobx'
import ReactDOMServer from 'react-dom/server'
import InfoWindow from '../components/modals/InfoWindow'
import Overlay from '../models/Overlay'

export default class MapElementStore {
    @observable map;

    @observable infoWindow;

    @observable resultPoiMarkerList = [];

    @observable resultAddressMarkerList = [];

    @observable resultAddressPolygonList = [];

    @observable selectedId = '';

    @action setMap(map) {
      this.map = map
    }

    @action setResultPoiMarker(poiList) {
      const markerList = []
      poiList.forEach((poi) => {
        const marker = new window.olleh.maps.overlay.Marker({
          position: new window.olleh.maps.LatLng(poi.point.lat, poi.point.lng),
          title: poi.getName(),
          map: this.map,
        })
        markerList.push(new Overlay(poi.getId(), marker))
      })
      this.resultPoiMarkerList = markerList
    }

    @action getResultPoiMarker(index) {
      return this.resultPoiMarkerList[index].overlay
    }

    @action setResultAddressMakerList(marker) {
      this.resultAddressMarkerList.push(marker)
    }

    @action getResultAddressMarker(index) {
      return this.resultAddressMarkerList[index].overlay
    }

    @action setResultAddressPolygonList(marker) {
      this.resultAddressPolygonList.push(marker)
    }

    @action getResultAddressPolygon(index) {
      return this.resultAddressPolygonList[index].overlay
    }

    @action setResultAddressMarkerPolygonList(addressList) {
      addressList.forEach((address) => {
        address.parcelAddress.forEach((parcelAddr, index) => {
          const { point, shape } = parcelAddr.geographicInformation
          if (point !== undefined) {
            const position = new window.olleh.maps.LatLng(point.lat, point.lng)
            const title = parcelAddr.fullAddress
            const marker = new window.olleh.maps.overlay.Marker({
              id: parcelAddr.pnucode,
              position,
              title,
              map: this.map,
            })
            this.setResultAddressMakerList(new Overlay(index, marker))
          }
          if (shape !== undefined) {
            const { type } = shape
            if (type === 'MultiLineString') {
              shape.coordinates.forEach((coordinate) => {
                const paths = []
                coordinate.forEach((inner) => {
                  paths.push(new window.olleh.maps.LatLng(inner[1], inner[0]))
                })
                const polyline = new window.olleh.maps.vector.Polyline({
                  path: new window.olleh.maps.Path(paths),
                  strokeColor: 'blue',
                  strokeOpacity: 1,
                  strokeWeight: 3,
                })
                this.setResultAddressPolygonList(new Overlay(index, polyline))
              })
            } else if (type === 'MultiPolygon') {
              shape.coordinates.forEach((coordinate) => {
                const paths = []
                coordinate[0].forEach((inner) => {
                  paths.push(new window.olleh.maps.LatLng(inner[1], inner[0]))
                })
                const polygon = new window.olleh.maps.vector.Polygon({
                  paths: new window.olleh.maps.Path(paths),
                  strokeColor: 'blue',
                  strokeOpacity: 1,
                  strokeWeight: 3,
                  fillColor: 'blue',
                  fillOpacity: 0.5,
                })
                this.setResultAddressPolygonList(new Overlay(index, polygon))
              })
            }
          }
        })

        address.roadAddress.forEach((roadAddr, index) => {
          const { point, shape } = roadAddr.geographicInformation
          if (point !== undefined) {
            const position = new window.olleh.maps.LatLng(point.lat, point.lng)
            const title = roadAddr.fullAddress
            const marker = new window.olleh.maps.overlay.Marker({
              id: roadAddr.rbucode,
              position,
              title,
              map: this.map,
            })
            this.setResultAddressMakerList(new Overlay(index, marker))
          }
          if (shape !== undefined) {
            const { type } = shape
            if (type === 'MultiLineString') {
              shape.coordinates.forEach((coordinate) => {
                const paths = []
                coordinate.forEach((inner) => {
                  paths.push(new window.olleh.maps.LatLng(inner[1], inner[0]))
                })
                const polyline = new window.olleh.maps.vector.Polyline({
                  path: new window.olleh.maps.Path(paths),
                  strokeColor: 'red',
                  strokeOpacity: 1,
                  strokeWeight: 3,
                })
                this.setResultAddressPolygonList(new Overlay(index, polyline))
              })
            } else if (type === 'MultiPolygon') {
              shape.coordinates.forEach((coordinate) => {
                const paths = []
                coordinate[0].forEach((inner) => {
                  paths.push(new window.olleh.maps.LatLng(inner[1], inner[0]))
                })
                const polygon = new window.olleh.maps.vector.Polygon({
                  paths: new window.olleh.maps.Path(paths),
                  strokeColor: 'red',
                  strokeOpacity: 1,
                  strokeWeight: 3,
                  fillColor: 'red',
                  fillOpacity: 0.5,
                })
                this.setResultAddressPolygonList(new Overlay(index, polygon))
              })
            }
          }
        })
      })
    }

    @action handleResultList(targetList, actionType, targetName) {
      if (targetList.length !== 0) {
        if (actionType === 'show') {
          targetList.forEach((item) => {
            item.overlay.setMap(this.map)
          })
        } else {
          targetList.forEach((item) => {
            item.overlay.setMap(null)
            item.overlay.erase()
          })
          if (actionType === 'erase') {
            if (targetName === 'marker') {
              this.resultPoiMarkerList = []
            } else if (targetName === 'addressMarker') {
              this.resultAddressMarkerList = []
            } else {
              this.resultAddressPolygonList = []
            }
          }
        }
      }
    }

    @action setInfoWindow(infoWindow) {
      if (this.infoWindow !== undefined) {
        this.infoWindow.close()
      }
      this.infoWindow = infoWindow
    }

    @action setWindow = (event, index, poi) => {
      const infoWindow = new window.olleh.maps.overlay.InfoWindow({
        content: ReactDOMServer.renderToStaticMarkup(<InfoWindow poi={poi} />),
        padding: 0,
      })
      this.setInfoWindow(infoWindow)
      if (index !== '') {
        infoWindow.open(this.map, this.getResultPoiMarker(index))
      } else {
        infoWindow.open(this.map, event.getSource())
      }
    }

    @action fitMap = (type, list) => {
      let lb; let rt
      if (list[0] !== undefined) {
        if (type === 'marker') {
          const firstItem = list[0].overlay.getPosition()
          const minMaxLng = [firstItem.x, firstItem.x]
          const minMaxLat = [firstItem.y, firstItem.y]
          list.forEach((item) => {
            const marker = item.overlay
            const position = marker.getPosition()
            if (position.y < minMaxLat[0]) {
              minMaxLat[0] = position.y
            } else if (position.y > minMaxLat[1]) {
              minMaxLat[1] = position.y
            }
            if (position.x < minMaxLng[0]) {
              minMaxLng[0] = position.x
            } else if (position.x > minMaxLng[1]) {
              minMaxLng[1] = position.x
            }
          })

          lb = new window.olleh.maps.UTMK(minMaxLng[0], minMaxLat[0]) // 좌측 하단 좌표
          rt = new window.olleh.maps.UTMK(minMaxLng[1], minMaxLat[1]) // 우측 상단 좌표
        } else {
          const { overlay } = list[0]
          const firstItem = (overlay.getPaths !== undefined)
            // eslint-disable-next-line no-underscore-dangle
            ? overlay.getPaths()[0]._coords[0] : overlay.getPath()._coords[0]
          const minMaxLng = [firstItem.x, firstItem.x]
          const minMaxLat = [firstItem.y, firstItem.y]
          list.forEach((item) => {
            const innerOverlay = item.overlay
            innerOverlay.setMap(this.map)
            const coords = (overlay.getPaths !== undefined)
              // eslint-disable-next-line no-underscore-dangle
              ? innerOverlay.getPaths()[0]._coords : innerOverlay.getPath()._coords
            coords.forEach((position) => {
              if (position.y < minMaxLat[0]) {
                minMaxLat[0] = position.y
              } else if (position.y > minMaxLat[1]) {
                minMaxLat[1] = position.y
              }
              if (position.x < minMaxLng[0]) {
                minMaxLng[0] = position.x
              } else if (position.x > minMaxLng[1]) {
                minMaxLng[1] = position.x
              }
            })
          })

          lb = new window.olleh.maps.LatLng(minMaxLat[0], minMaxLng[0]) // 좌측 하단 좌표
          rt = new window.olleh.maps.LatLng(minMaxLat[1], minMaxLng[1]) // 우측 상단 좌표
        }
        if (lb !== undefined && rt !== undefined) {
          const bounds = new window.olleh.maps.Bounds(lb, rt)
          this.map.fitBounds(bounds)
        }
      }
    }
}
