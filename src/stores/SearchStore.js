import { observable, action } from 'mobx'
import integratedSearchRepository from '../repositories/IntegratedSearchRepository'
import SuggestResult from '../models/SuggestResult'
import Category from '../models/Category'
import Address from '../models/Address'
import PoiResult from '../models/PoiResult'
import AddressResult from '../models/AddressResult'
import AnalyzedResult from '../models/AnalyzedResult'
import dataSearchRepository from '../repositories/DataSearchRepository'

export default class SearchStore {
    managementStore;

    mapElementStore;

    searchResultStore;

    constructor(managementStore, mapElementStore, searchResultStore) {
      this.managementStore = managementStore
      this.mapElementStore = mapElementStore
      this.searchResultStore = searchResultStore
    }

    @observable terms = '';

    @observable options = { _evaluate: true };

    @observable integratedOptions = {
      mode: 'NAVIGATION', children: 'OFF', autoCorrection: true, categoryOnly: true, match: true,
    };

    @observable dataOptions = {};

    @action setTerms(newTerms) {
      this.terms = newTerms
      this.options.terms = newTerms
      this.integratedOptions.start = 0
      this.searchResultStore.showPage = 0
    }

    @action setOptions(key, value) {
      if (key === 'address.code' || key === 'category.code') {
        this.options[key] = value
      } else if (this.searchResultStore.searchMode === 'integrated') {
        this.integratedOptions[key] = value
      } else {
        this.dataOptions[key] = value
      }
    }

    @action
    get = uuid => integratedSearchRepository.get(uuid, this.managementStore.getHeaders())

    @action
    search = (callBackFun) => {
      let option = this.options
      this.searchResultStore.searching = true
      this.searchResultStore.result.poiList = []
      this.searchResultStore.result.addressList = []

      let targetRepository = integratedSearchRepository
      if (this.searchResultStore.searchMode === 'data') {
        targetRepository = dataSearchRepository
        option = { ...option, ...this.dataOptions }
      } else {
        if (this.integratedOptions.start === undefined || this.integratedOptions.start === 0) {
          const point = window.olleh.maps.LatLng.valueOf(
            new window.olleh.maps.UTMK(this.mapElementStore.map.getCenter().x,
              this.mapElementStore.map.getCenter().y)
          )
          option['point.lat'] = point.y
          option['point.lng'] = point.x
        }
        option = { ...option, ...this.integratedOptions }
      }

      targetRepository.search(option, this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          this.searchResultStore.searchOnce = true
          this.searchResultStore.searching = false
          this.mapElementStore.handleResultList(this.mapElementStore.resultPoiMarkerList, 'erase', 'marker')
          this.mapElementStore.handleResultList(this.mapElementStore.resultAddressMarkerList, 'erase', 'addressMarker')
          this.mapElementStore.handleResultList(this.mapElementStore.resultAddressPolygonList, 'erase', 'addressPolygon')

          this.searchResultStore.numberOfPois = response.data.numberOfPois
          response.data.pois.forEach((poi) => {
            const poiResult = new PoiResult(poi)
            this.searchResultStore.result.poiList.push(poiResult)
          })
          this.mapElementStore.setResultPoiMarker(this.searchResultStore.result.poiList)
          if (this.searchResultStore.searchMode !== 'data' && response.data.residentialAddress !== undefined) {
            response.data.residentialAddress.forEach((address, index) => {
              this.searchResultStore.result.addressList.push(new AddressResult(index, address))
            })
            this.mapElementStore.setResultAddressMarkerPolygonList(
              this.searchResultStore.result.addressList
            )
            this.searchResultStore.numberOfAddress = response.data.numberOfAddress
          } else {
            this.searchResultStore.result.addressList = []
            this.searchResultStore.numberOfAddress = 0
          }
          if (this.searchResultStore.numberOfPois === 0
            && this.searchResultStore.numberOfAddress !== 0) {
            this.searchResultStore.resultMode = 'address'
            this.searchResultStore.resultPath = '주소 검색 결과'
          } else {
            this.searchResultStore.resultMode = 'poi'
            this.searchResultStore.resultPath = '장소 검색 결과'
          }
          if (response.data.evaluation !== undefined) {
            this.searchResultStore.analyzedResult = new AnalyzedResult(response.data.evaluation)
          }
          // eslint-disable-next-line no-underscore-dangle
          this.options._evaluate = true
          if (callBackFun !== undefined) callBackFun()
        } else if (response.status === 400) {
          // eslint-disable-next-line no-underscore-dangle
          this.options._evaluate = null
          this.search(callBackFun)
        } else {
          this.searchResultStore.searching = false
        }
      })
    }

    @action
    autocomplete = () => {
      if (this.terms.trim() !== '') {
        integratedSearchRepository.autocomplete({ terms: this.terms },
          this.managementStore.getHeaders()).then((response) => {
          if (response.status === 200) {
            this.searchResultStore.result.suggestList = []
            response.data.suggests.forEach(suggest => (
              this.searchResultStore.result.suggestList.push(
                new SuggestResult(suggest.terms, suggest.parcelAddresses)
              )))
          }
        })
      }
    }

    @action
    geocode = (address) => {
      this.searchResultStore.geocoding = true
      integratedSearchRepository.geocode({ addressText: address },
        this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          this.searchResultStore.geocoding = false
          this.searchResultStore.result.geocodeList = []
          console.log(response.data.residentialAddress)
          response.data.residentialAddress.forEach(
            (addr, index) => {
              this.searchResultStore.result.geocodeList.push(new AddressResult(index, addr))
              console.log(addr)
            }
          )
          console.log(this.searchResultStore.result.geocodeList)
        }
      })
    }

    @action
    getCategoryList = (type, parentCode) => {
      const parameter = { type }
      if (parentCode !== '') {
        parameter.parentCode = parentCode
      }
      if (type === 'master' && this.searchResultStore.categoryList.master.length !== 0) {
        return
      }
      if (type === 'master') {
        this.searchResultStore.getStep.first = true
      } else if (type === 'middle') {
        this.searchResultStore.getStep.second = true
        this.searchResultStore.categoryList.middle = []
        this.searchResultStore.categoryList.sub = []
      } else {
        this.searchResultStore.getStep.third = true
        this.searchResultStore.categoryList.sub = []
      }
      integratedSearchRepository.getCategory(parameter,
        this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          if (type === 'master') {
            this.searchResultStore.getStep.first = false
          } else if (type === 'middle') {
            this.searchResultStore.getStep.second = false
          } else if (type === 'sub') {
            this.searchResultStore.getStep.third = false
          }
          response.data.categoryList.forEach(
            category => this.searchResultStore.categoryList[type].push(
              new Category(category.code, category.name)
            )
          )
        }
      })
    }

    @action
    getAddressList = (type, parentCode) => {
      const parameter = { code: parentCode }
      if (type === undefined && parentCode === undefined
        && this.searchResultStore.addressList.siDo.length !== 0) {
        return
      }
      if (type === undefined) {
        this.searchResultStore.getStep.first = true
      } else if (type === 'siDo') {
        this.searchResultStore.getStep.second = true
        this.searchResultStore.addressList.siGunGu = []
        this.searchResultStore.addressList.eupMyeonDong = []
      } else {
        this.searchResultStore.getStep.third = true
        this.searchResultStore.addressList.eupMyeonDong = []
      }
      integratedSearchRepository.getStepByStep(parameter,
        this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          if (type === undefined) {
            this.searchResultStore.getStep.first = false
          } else if (type === 'siDo') {
            this.searchResultStore.getStep.second = false
          } else {
            this.searchResultStore.getStep.third = false
          }
          response.data.address.forEach(address => (
            // eslint-disable-next-line no-nested-ternary
            address.next === 'SIGUNGU' || address.code === '3600000000'
              ? this.searchResultStore.addressList.siDo.push(
                new Address(address.code, address.siDo)
              )
              : address.next === 'EUPMYEONDONG'
                ? this.searchResultStore.addressList.siGunGu.push(
                  new Address(address.code, address.siGunGu)
                )
                : this.searchResultStore.addressList.eupMyeonDong.push(
                  new Address(address.code, address.eupMyeonDong)
                )
          ))
        }
      })
    }

    @action
    edit = (callBackFun) => {
      const { id } = this.searchResultStore.poi
      dataSearchRepository.edit(id, this.setEditorParameter(),
        this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          if (response.data.statusMessage === 'UPDATED') {
            this.searchResultStore.updated = true
          }
          if (callBackFun !== undefined) callBackFun()
        }
      })
    }

    @action
    insert = (callBackFun) => {
      const { id } = this.searchResultStore.poi
      dataSearchRepository.insert(id, this.setEditorParameter(),
        this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          if (response.data.statusMessage === 'CREATED') {
            this.searchResultStore.updated = true
          }
          if (callBackFun !== undefined) callBackFun()
        }
      })
    }

    @action
    delete = (id, callBackFun) => {
      dataSearchRepository.delete(id, this.managementStore.getHeaders()).then((response) => {
        if (response.status === 200) {
          console.log(response.data)
          if (response.data.statusMessage === 'DELETED') {
            this.searchResultStore.updated = true
          }
          if (callBackFun !== undefined) callBackFun()
        }
      })
    }

    setEditorParameter() {
      const parameter = {}
      parameter.terms = this.searchResultStore.poi.name
      if (this.searchResultStore.poi.branch !== undefined) {
        parameter.branch = this.searchResultStore.poi.branch
      }
      if (this.searchResultStore.poi.newCategory !== undefined) {
        parameter.categoryObject = this.searchResultStore.poi.newCategory
      }
      if (this.searchResultStore.poi.newAddress !== undefined) {
        parameter.addressObject = {
          siDo: this.searchResultStore.poi.newAddress.siDo,
          siGunGu: this.searchResultStore.poi.newAddress.siGunGu,
          eupMyeonDong: this.searchResultStore.poi.newAddress.eupMyeonDong,
          haengJeongDong: this.searchResultStore.poi.newAddress.haengJeongDong,
          eupMyeon: this.searchResultStore.poi.newAddress.eupMyeon,
          ri: this.searchResultStore.poi.newAddress.ri,
          houseNumber: this.searchResultStore.poi.newAddress.houseNumber,
          street: this.searchResultStore.poi.newAddress.street,
          streetNumber: this.searchResultStore.poi.newAddress.streetNumber,
        }
        parameter.point = {
          lat: this.searchResultStore.poi.newAddress.lat,
          lng: this.searchResultStore.poi.newAddress.lng,
        }
      }
      if (this.searchResultStore.poi.phones !== undefined) {
        parameter.phone = {
          representation: this.searchResultStore.poi.getRepresentationPhone(),
          normal: this.searchResultStore.poi.getNormalPhone(),
          fax: this.searchResultStore.poi.getFax(),
        }
      }
      console.log(parameter)
      return parameter
    }
}
