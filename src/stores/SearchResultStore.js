import { observable, action } from 'mobx'

export default class SearchResultStore {
  @observable searchOnce = false

  @observable searching = false

  @observable geocoding = false

  @observable getStep = { first: false, second: false, third: false }

  @observable searchMode = 'integrated' // integrated, data

  @observable resultMode = 'poi' // poi, address

  @observable showPage = 0

  @observable resultPath = '장소 검색 결과'; // 장소 검색 결과, 주소 검색 결과

  @observable anchorEl = {
    filter: null, category: null, analyzer: null, result: null,
  }

  @observable result = {
    suggestList: [], poiList: [], addressList: [], geocodeList: [],
  }

  @observable addressPath = ''

  @observable numberOfPois = 0

  @observable numberOfAddress = 0

  @observable selectedB2CFilters = true

  @observable selectedCategory = {
    master: { code: 'all', name: 'all' },
    middle: { code: 'all', name: 'all' },
    sub: { code: 'all', name: 'all' },
  }

  @observable selectedAddress = {
    siDo: { code: 'all', name: 'all' },
    siGunGu: { code: 'all', name: 'all' },
    eupMyeonDong: { code: 'all', name: 'all' },
  }

  @observable categoryList= {
    master: [],
    middle: [],
    sub: [],
  }

  @observable addressList= {
    siDo: [],
    siGunGu: [],
    eupMyeonDong: [],
  };

  // for editor
  @observable poi;

  @observable editModalOpen = false

  @observable deleteModalOpen = false

  @observable updated = false

  // for analyzer

  @observable analyzedResult;

  @action getCategoryPath= () => {
    let path = ''
    if (this.selectedCategory.master.name !== 'all') {
      path = this.selectedCategory.master.name
    }
    if (this.selectedCategory.middle.name !== 'all') {
      path += ` > ${this.selectedCategory.middle.name}`
    }
    if (this.selectedCategory.sub.name !== 'all') {
      path += ` > ${this.selectedCategory.sub.name}`
    }
    return path
  }

  @action getAddressPath= () => {
    if (this.addressPath !== '') {
      return this.addressPath
    }
    let path = ''
    if (this.selectedAddress.siDo.name !== 'all') {
      path = this.selectedAddress.siDo.name
    }
    if (this.selectedAddress.siGunGu.name !== 'all') {
      path += ` > ${this.selectedAddress.siGunGu.name}`
    }
    if (this.selectedAddress.eupMyeonDong.name !== 'all') {
      path += ` > ${this.selectedAddress.eupMyeonDong.name}`
    }
    return path
  }

  @action setAddressList(key, value) {
    this.addressList[key] = value
  }

  @action resetSelectedCategory= () => {
    this.selectedCategory = {
      master: { code: 'all', name: 'all' },
      middle: { code: 'all', name: 'all' },
      sub: { code: 'all', name: 'all' },
    }
  }

  @action resetSelectedAddress= () => {
    this.selectedAddress = {
      siDo: { code: 'all', name: 'all' },
      siGunGu: { code: 'all', name: 'all' },
      eupMyeonDong: { code: 'all', name: 'all' },
    }
    this.addressPath = ''
  }

  @action clearStore = () => {
    this.searchOnce = false
    this.searching = false

    this.getStep = { first: false, second: false, third: false }

    this.searchMode = 'integrated'
    this.resultMode = 'poi'

    this.resultPath = '장소 검색 결과'

    this.anchorEl = {
      filter: null, category: null, analyzer: null, result: null,
    }

    this.result = {
      suggestList: [], poiList: [], addressList: [], geocodeList: [],
    }

    this.addressPath = ''

    this.numberOfPois = 0
    this.numberOfAddress = 0

    this.selectedB2CFilters = true
    this.selectedCategory = {
      master: { code: 'all', name: 'all' },
      middle: { code: 'all', name: 'all' },
      sub: { code: 'all', name: 'all' },
    }
    this.selectedAddress = {
      siDo: { code: 'all', name: 'all' },
      siGunGu: { code: 'all', name: 'all' },
      eupMyeonDong: { code: 'all', name: 'all' },
    }
    this.categoryList = {
      master: [],
      middle: [],
      sub: [],
    }
    this.addressList = {
      siDo: [],
      siGunGu: [],
      eupMyeonDong: [],
    }
  }
}
