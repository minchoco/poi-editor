import { observable, action } from 'mobx'
import managementRepository from '../repositories/ManagementRepository'
import dataSearchRepository from '../repositories/DataSearchRepository'
import integratedSearchRepository from '../repositories/IntegratedSearchRepository'

export default class ManagementStore {
    // login
    @observable loginStatus = false;

    @observable loginStatusMessage = '';

    @observable authKey = '';

    @observable nickName = '';

    @observable serverZone = 'b2c';

    @observable environment = 'prd';

    @observable editor = false;

    @observable admin = true;

    @observable statistics = [];

    @action
    getAuthKey = () => this.authKey

    setEnvironment = (env) => {
      this.environment = env
      if (env === 'prd') {
        managementRepository.setBaseUrl(process.env.BASE_URL)
        integratedSearchRepository.setBaseUrl(process.env.BASE_URL)
        dataSearchRepository.setBaseUrl(process.env.BASE_URL)
      } else {
        managementRepository.setBaseUrl(process.env.BASE_DEV_URL)
        integratedSearchRepository.setBaseUrl(process.env.BASE_DEV_URL)
        dataSearchRepository.setBaseUrl(process.env.BASE_DEV_URL)
      }
    }

    @action
    getHeaders = () => ({
      'Content-Type': 'application/json;charset=UTF-8',
      Authorization: this.getAuthKey(),
      'X-Server-Zone': this.serverZone,
    })

    @action
    checkKey = (...params) => {
      if (params[0] === 'test') {
        managementRepository.setBaseUrl('http://localhost:9000')
        integratedSearchRepository.setBaseUrl('http://localhost:9000')
        dataSearchRepository.setBaseUrl('http://localhost:9000')
        this.serverZone = 'local'
        this.environment = ''
      }
      this.authKey = decodeURI(params[0])
      managementRepository.checkKey(this.getHeaders()).then((response) => {
        if (response.status === 200) {
          this.loginStatus = true
          this.nickName = response.data.groupName.replace('[', '').replace(']', '')
          if (this.nickName === 'test' || this.nickName === 'VOC') {
            this.admin = true
          } else if (this.nickName === 'MAPPERS') {
            this.editor = true
          }
        }
      }).catch((err) => {
        if (err.message === 'Network Error') {
          this.loginStatusMessage = '네트워크상태를 확인해주세요.'
        } else {
          this.loginStatusMessage = '서버에 접속할 수 없습니다.'
        }
      })
    }

    @action
    getStatistics = () => {
      managementRepository.getStatistics(this.getHeaders()).then((response) => {
        if (response.status === 200) {
          this.statistics = response.data.statistics
          return this.statistics
        }
        return null
      })
    }

    @action
    clearStore = () => {
      this.loginStatus = false
      this.loginStatusMessage = ''
      this.authKey = ''
      this.nickName = ''
      this.serverZone = 'b2c'
      this.environment = 'prd'
      this.editor = false
      this.admin = false
    }
}
