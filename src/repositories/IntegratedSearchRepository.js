import axios from 'axios'

class SearchRepository {
    BASE_URL = process.env.BASE_URL

    ROOT_URL = '/search/v1.0'

    SEARCH = '/pois'

    AUTOCOMPLETE = '/utilities/autocomplete'

    GEOCODE= '/utilities/geocode'

    CATEGORY = '/utilities/category'

    STEP_BY_STEP = '/utilities/stepbystep'

    setBaseUrl = (url) => {
      this.BASE_URL = url
    }

    get(uuid, headers) {
      return axios.get(`${this.BASE_URL + this.ROOT_URL + this.SEARCH}/${uuid}`, { headers })
    }

    search(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.SEARCH, { params, headers })
    }

    autocomplete(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.AUTOCOMPLETE, { params, headers })
    }

    geocode(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.GEOCODE, { params, headers })
    }

    getCategory(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.CATEGORY, { params, headers })
    }

    getStepByStep(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.STEP_BY_STEP, { params, headers })
    }
}

const searchRepository = new SearchRepository()

export default searchRepository
