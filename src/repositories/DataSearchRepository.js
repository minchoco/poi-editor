import axios from 'axios'

class DataSearchRepository {
    BASE_URL = process.env.BASE_URL

    ROOT_URL = '/management/_node'

    DATA_SEARCH = '/data/pois'

    setBaseUrl = (url) => {
      this.BASE_URL = url
    }

    search(params, headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.DATA_SEARCH, { params, headers })
    }

    edit(id, params, headers) {
      return axios.post(`${this.BASE_URL + this.ROOT_URL + this.DATA_SEARCH}/${id}`, params, { headers })
    }

    insert(id, params, headers) {
      return axios.put(this.BASE_URL + this.ROOT_URL + this.DATA_SEARCH, params, { headers })
    }

    delete(id, headers) {
      return axios.delete(`${this.BASE_URL + this.ROOT_URL + this.DATA_SEARCH}/${id}`, { headers })
    }
}

const dataRepository = new DataSearchRepository()

export default dataRepository
