import axios from 'axios'

class ManagementRepository {
    BASE_URL = process.env.BASE_URL

    ROOT_URL = '/management/_node'

    MANAGE_POI = '/authorization/_check'

    STATISTICS = '/statistics'

    setBaseUrl = (url) => {
      this.BASE_URL = url
    }

    checkKey(headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.MANAGE_POI, { headers })
    }

    getStatistics(headers) {
      return axios.get(this.BASE_URL + this.ROOT_URL + this.STATISTICS, { headers })
    }
}

const managementRepository = new ManagementRepository()

export default managementRepository
