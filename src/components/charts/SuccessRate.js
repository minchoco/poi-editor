import React, { PureComponent } from 'react'
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, LabelList,
} from 'recharts'
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import {
  Typography, Paper, Grid, Table, TableHead, TableRow, TableCell, TableBody,
} from '@material-ui/core'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 20,
    color: theme.palette.text.secondary,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
})

// eslint-disable-next-line react/prop-types
const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload !== null) {
    return (
      <div style={{ backgroundColor: 'white', padding: '10px' }}>
        <p>{`${label} ${payload[0].payload.date.split('T')[1]}`}</p>
        <p style={{ color: 'blue' }}>{` 성공률 : ${payload[0].payload.sRate}%`}</p>
        <p style={{ color: 'red' }}>{` 실패율 : ${payload[0].payload.fRate}%`}</p>
      </div>
    )
  }

  return null
}

@inject('managementStore')
@observer
class SuccessRate extends PureComponent {
  componentDidMount() {
    const { managementStore } = this.props
    managementStore.getStatistics()
  }

  render() {
    const {
      classes, managementStore,
    } = this.props

    if (!managementStore.loginStatus) {
      return <Redirect to='/tools' />
    }

    return (
      <Grid
        container={true} spacing={24} alignItems='center'
        justify='center' className={classes.root}>
        <Paper className={classes.paper}>
          <Typography component='h5' variant='h5' style={{ margin: '15px' }}>
            검색성공률
          </Typography>
          <Grid container={true} justify='center' alignItems='center'>
            <Grid item={true} xs={12} justify='center'>
              <BarChart
                width={1000}
                height={300}
                data={managementStore.statistics}
                stackOffset='sign'
                style={{ margin: 'auto' }}>
                <CartesianGrid strokeDasharray='3 3' />
                <XAxis dataKey='id' />
                <YAxis />
                <Tooltip content={<CustomTooltip />} />
                <Legend />
                <Bar
                  dataKey='success' stackId='a' fill='#50a1ce'>
                  <LabelList dataKey='sRate' position='center' />
                </Bar>
                <Bar
                  dataKey='fail' stackId='a' fill='#d26d75'>
                  <LabelList dataKey='fRate' position='center' />
                </Bar>
              </BarChart>
            </Grid>
            <Grid item={true} xs={12}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell align='center'>날짜</TableCell>
                    <TableCell align='center'>추출 시간</TableCell>
                    <TableCell align='center'>전체</TableCell>
                    <TableCell align='center'>검색 성공</TableCell>
                    <TableCell align='center'>검색 실패</TableCell>
                    <TableCell align='center'>검색 성공률</TableCell>
                    <TableCell align='center'>검색 실패율</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {managementStore.statistics.reverse().map(row => (
                    <TableRow key={row.id}>
                      <TableCell align='center' component='th' scope='row'>
                        {row.id}
                      </TableCell>
                      <TableCell align='center'>{row.date}</TableCell>
                      <TableCell align='center'>
                        {row.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        {'건'}
                      </TableCell>
                      <TableCell align='center'>
                        {row.success.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        {'건'}
                      </TableCell>
                      <TableCell align='center'>
                        {row.fail.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        {'건'}
                      </TableCell>
                      <TableCell align='center'>
                        {row.sRate}
                        {'%'}
                      </TableCell>
                      <TableCell align='center'>
                        {row.fRate}
                        {'%'}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    )
  }
}

SuccessRate.propTypes = {
  classes: PropTypes.object,
  managementStore: PropTypes.object,
}

export default withStyles(styles)(SuccessRate)
