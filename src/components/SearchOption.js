import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import { Button, Popover } from '@material-ui/core'

const styles = theme => ({
  popovers: {
    paddingLeft: '10px',
  },
  typography: {
    margin: theme.spacing.unit * 2,
  },
})

@inject('searchResultStore')
@observer
class SearhOption extends Component {
  handleClick = (event, type) => {
    const { searchResultStore } = this.props
    searchResultStore.anchorEl[type] = event.currentTarget
  };

  handleClose = (event, type) => {
    const { searchResultStore } = this.props
    searchResultStore.anchorEl[type] = null
  };

  render() {
    const {
      classes, searchResultStore, type, contents, defaultTitle,
    } = this.props
    const targetAnchorEl = searchResultStore.anchorEl[type]
    const open = Boolean(targetAnchorEl)
    let path

    if (type === 'filter') {
      path = defaultTitle
    } else if (type === 'category') {
      path = searchResultStore.getCategoryPath()
    } else if (type === 'address') {
      path = searchResultStore.getAddressPath()
    } else if (type === 'analyzer') {
      path = defaultTitle
    } else if (type === 'result') {
      path = searchResultStore.resultPath
    }

    return (
      <div className={classes.popovers}>
        <Button
          aria-owns={open ? 'popover' : undefined}
          aria-haspopup='true'
          onClick={event => this.handleClick(event, type)}
          variant={path !== '' ? 'contained' : 'outlined'}
          color={type === 'analyzer' ? 'secondary' : path !== '' && 'primary'}>
          {path !== '' ? path : defaultTitle }
        </Button>
        <Popover
          id='popover'
          open={open}
          anchorEl={targetAnchorEl}
          onClose={event => this.handleClose(event, type)}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}>
          {contents}
        </Popover>
      </div>
    )
  }
}

SearhOption.propTypes = {
  classes: PropTypes.object.isRequired,
  contents: PropTypes.string,
  defaultTitle: PropTypes.string,
  searchResultStore: PropTypes.object,
  type: PropTypes.string,
}

export default withStyles(styles)(SearhOption)
