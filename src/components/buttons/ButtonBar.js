import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import PropTypes from 'prop-types'
import {
  IconButton, Menu, MenuItem, Modal,
} from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import Editor from '../modals/Editor'
import Deletor from '../modals/Deletor'

@inject('searchResultStore')
@observer
class ButtonBar extends Component {
  state = {
    anchorEl: null,
    editModalOpen: false,
    deleteModalOpen: false,
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget })
  };

  handleClose = () => {
    this.setState({ anchorEl: null })
  };

  handleEditModalOpen = () => {
    const { searchResultStore } = this.props
    searchResultStore.editModalOpen = true
    this.setState({ editModalOpen: true })
  }

  handleDeleteModalOpen = () => {
    const { searchResultStore } = this.props
    searchResultStore.deleteModalOpen = true
    this.setState({ deleteModalOpen: true })
  }

  handleModalClose = () => {
    const { searchResultStore } = this.props
    searchResultStore.editModalOpen = false
    searchResultStore.deleteModalOpen = false
    this.setState({ deleteModalOpen: false })
  }

  render() {
    const { anchorEl, editModalOpen, deleteModalOpen } = this.state
    const { poi, searchResultStore } = this.props
    const open = Boolean(anchorEl)

    return (
      <div>
        <IconButton
          aria-label='More'
          aria-owns={open ? 'long-menu' : undefined}
          aria-haspopup='true'
          onClick={this.handleClick}>
          <MoreVertIcon />
        </IconButton>
        <Menu
          id='long-menu'
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              width: 200,
            },
          }}>
          <MenuItem
            key='edit' onClick={this.handleEditModalOpen} name='edit'>
          수정
          </MenuItem>
          <MenuItem
            key='delete' onClick={this.handleDeleteModalOpen} name='delete'>
          삭제
          </MenuItem>
        </Menu>
        <Modal
          aria-labelledby='editor-modal'
          open={editModalOpen && searchResultStore.editModalOpen}
          onClose={this.handleModalClose}>
          <Editor poi={poi} type='U' />
        </Modal>
        <Modal
          aria-labelledby='editor-modal'
          open={deleteModalOpen && searchResultStore.deleteModalOpen}
          onClose={this.handleModalClose}>
          <Deletor poi={poi} />
        </Modal>
      </div>
    )
  }
}


ButtonBar.propTypes = {
  poi: PropTypes.object,
  searchResultStore: PropTypes.object,
}

export default ButtonBar
