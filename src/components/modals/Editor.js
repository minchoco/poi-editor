import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import withStyles from '@material-ui/core/styles/withStyles'
import PropTypes from 'prop-types'
import {
  TextField, Grid, Button, FormControl, Stepper, Step, StepLabel, Typography, FormHelperText,
} from '@material-ui/core'
import ArrowRight from '@material-ui/icons/ArrowRight'
import PoiResult from '../../models/PoiResult'
import CategoryList from '../lists/CategoryList'
import GeocodeSearch from '../GeocodeSearch'

const styles = theme => ({
  modal: {
    top: '5%',
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  item: {
    paddingTop: '0px',
  },
  form: {
    display: 'block',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  longFormControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    width: '60%',
  },
  formButton: {
    margin: theme.spacing.unit,
  },
  editForm: {
    border: '1px dashed red',
    flexDirection: 'column',
  },
  editFormControl: {
    display: 'block',
  },
  bigContainer: {
    width: '80%',
  },
  stepContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  activePage: {
    width: '800px',
  },
})

const getSteps = () => [
  'POI details',
  'Confirm',
  'Done',
]

@inject('searchStore', 'searchResultStore')
@observer
class Editor extends Component {
    state = {
      activeStep: 0,
      openEditCategory: false,
      openEditAddress: false,
      originName: '',
      originBranch: '',
      originCategory: '',
      originRoadAddress: '',
      originParcelAddress: '',
      originalRepresentationPhone: '',
      originalNormalPhone: '',
      originalFax: '',
      editedCategory: 'none', // 'done', 'none', 'error'
      editedAddress: 'none', // 'done', 'none', 'error'
      message: '',
      nameError: false,
      categoryError: false,
      addressError: false,
    };

    componentDidMount() {
      const { poi, searchResultStore } = this.props

      if (poi.id === undefined) {
        searchResultStore.poi = new PoiResult()
      } else {
        searchResultStore.poi = poi
      }
      this.setState({
        originName: searchResultStore.poi.name,
        originBranch: searchResultStore.poi.branch,
        originCategory: searchResultStore.poi.getCategory(),
        originRoadAddress: searchResultStore.poi.getRoadAddress(),
        originParcelAddress: searchResultStore.poi.getParcelAddress(),
        originalRepresentationPhone: searchResultStore.poi.getRepresentationPhone(),
        originalNormalPhone: searchResultStore.poi.getNormalPhone(),
        originalFax: searchResultStore.poi.getFax(),
      })
    }

    componentWillUnmount() {
      const { searchStore, searchResultStore } = this.props
      if (searchResultStore.updated) {
        searchStore.search()
      }
    }

    openEditCategory = () => {
      const { type, searchResultStore } = this.props
      const { openEditCategory, originCategory } = this.state
      this.setState({ openEditCategory: !openEditCategory })
      if (searchResultStore.poi.validCategory(searchResultStore.poi.newCategory)) {
        searchResultStore.poi.category = searchResultStore.poi.newCategory
      } else if (searchResultStore.poi.newCategory !== undefined) {
        this.setState({ editedCategory: 'error', message: '유효하지 않은 카테고리입니다.' })
      }
      if (originCategory !== searchResultStore.poi.getCategory()) {
        if (type === 'I') {
          this.setState({ categoryError: false })
        }
        this.setState({ editedCategory: 'done' })
      }
    }

    openEditAddress = () => {
      const { type, searchResultStore } = this.props
      const { openEditAddress, originRoadAddress, originParcelAddress } = this.state
      this.setState({ openEditAddress: !openEditAddress })
      if (searchResultStore.poi.newAddress !== undefined) {
        if (!searchResultStore.poi.isDetailParcelAddress()) {
          this.setState({ addressError: true, message: '상세주소(번지)까지 입력되어야합니다.' })
          return
        }
        searchResultStore.poi.setAddress(searchResultStore.poi.newAddress)
        if (originRoadAddress !== searchResultStore.poi.getRoadAddress()) {
          this.setState({ editedAddress: 'done' })
        }
        if (originParcelAddress !== searchResultStore.poi.getParcelAddress()) {
          if (type === 'I') {
            this.setState({ addressError: false })
          }
          this.setState({ editedAddress: 'done' })
        }
      }
    }

    resetCategory = () => {
      const { searchResultStore } = this.props
      searchResultStore.poi.category = searchResultStore.poi.originalCategory
      this.setState({ editedCategory: 'none' })
    }

    resetAddress = () => {
      const { searchResultStore } = this.props
      searchResultStore.poi.address = searchResultStore.poi.originalAddress
      this.setState({ editedAddress: 'none' })
    }

    closeEditCategory = () => {
      this.setState({ openEditCategory: false })
    }

    closeEditAddress = () => {
      this.setState({ openEditAddress: false })
    }

    handleNext = () => {
      const { type, searchStore, searchResultStore } = this.props
      const { activeStep } = this.state

      if (activeStep === 0 && searchResultStore.poi !== undefined) {
        const nameError = type === 'I' && (searchResultStore.poi.name === undefined || searchResultStore.poi.name === '')
        const addressError = type === 'I' && searchResultStore.poi.address === undefined
        const categoryError = type === 'I' && searchResultStore.poi.category === undefined
        if (nameError || addressError || categoryError) {
          this.setState({
            nameError, addressError, categoryError, message: '이 항목은 반드시 입력해야 합니다.',
          })
          return
        }
      }
      if (activeStep === 1) {
        console.log('upsert', type)
        if (type === 'U') {
          searchStore.edit(() => {
            console.log('done', searchResultStore.updated)
            if (searchResultStore.updated) {
              this.setState({ message: '정상적으로 반영되었습니다.' })
            } else {
              this.setState({ message: '문제가 발생했습니다.' })
            }
          })
        } else {
          searchStore.insert(() => {
            console.log('done', searchResultStore.updated)
            if (searchResultStore.updated) {
              this.setState({ message: '정상적으로 반영되었습니다.' })
            } else {
              this.setState({ message: '문제가 발생했습니다.' })
            }
          })
        }
        console.log('upsert done')
      }
      this.setState(state => ({
        activeStep: state.activeStep + 1,
      }))
    };

    handleBack = () => {
      this.setState(state => ({
        activeStep: state.activeStep - 1,
      }))
    };

    handleReset = () => {
      this.setState({
        activeStep: 0,
      })
    };

    handleText = (e) => {
      const { searchResultStore, type } = this.props
      if (e.target.name === 'name') {
        searchResultStore.poi.name = e.target.value
        if (type === 'I' && e.target.value !== '') {
          this.setState({ nameError: false })
        }
      } else if (e.target.name === 'branch') {
        searchResultStore.poi.branch = e.target.value
      } else if (e.target.name === 'representationPhone') {
        searchResultStore.poi.setPhones('representation', e.target.value)
      } else if (e.target.name === 'normalPhone') {
        searchResultStore.poi.setPhones('normal', e.target.value)
      } else if (e.target.name === 'fax') {
        searchResultStore.poi.setPhones('fax', e.target.value)
      }
    }

    goToSearch = () => {
      const { searchResultStore } = this.props
      searchResultStore.editModalOpen = false
      searchResultStore.resetSelectedCategory()
    }

    stepActions() {
      const { type } = this.props
      const { activeStep } = this.state
      if (activeStep === 1) {
        if (type === 'U') {
          return '변경'
        }
        return '등록'
      } if (activeStep === 2) {
        return '종료'
      }
      return '다음'
    }

    render() {
      const {
        classes, poi, searchResultStore, type,
      } = this.props
      const {
        activeStep, nameError, categoryError, addressError, message,
        openEditCategory, editedCategory, openEditAddress, editedAddress,
        originName, originBranch, originCategory, originRoadAddress, originParcelAddress,
        originalRepresentationPhone, originalNormalPhone, originalFax,
      } = this.state
      const steps = getSteps()
      const buttonText = type === 'I' ? '등록' : '수정'

      return (
        <Grid
          container={true} alignItems='stretch' justify='center'
          className={classes.modal}>
          <Grid
            spacing={24} alignItems='center' justify='center'
            container={true} className={classes.grid}>
            <div className={classes.stepContainer}>
              <div className={classes.bigContainer}>
                <Stepper
                  classes={{ root: classes.stepper }}
                  activeStep={activeStep} alternativeLabel={true}>
                  {steps.map(label => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </div>
              { activeStep === 0 && (
                <form autoComplete='off'>
                  <Grid row={true} justify='center' className={classes.activePage}>
                    <Grid item={true} xs={12}>
                      <FormControl
                        required={true} error={nameError} className={classes.formControl}>
                        <TextField
                          id='name'
                          label='명칭 *'
                          placeholder="POI's Name"
                          variant='outlined'
                          defaultValue={poi.name}
                          name='name'
                          onChange={this.handleText} />
                        <FormHelperText>{nameError && message}</FormHelperText>
                      </FormControl>
                      <FormControl className={classes.formControl}>
                        <TextField
                          id='branch'
                          label='지점명'
                          placeholder="POI's branch"
                          variant='outlined'
                          defaultValue={poi.branch}
                          name='branch'
                          onChange={this.handleText} />
                      </FormControl>
                    </Grid>
                    <Grid item={true} xs={12}>
                      { openEditCategory
                        ? <CategoryList className={classes.editForm} isSearch={false} />
                        : (
                          <FormControl
                            required={true}
                            error={categoryError} className={classes.longFormControl}>
                            <TextField
                              id='category'
                              label='카테고리 *'
                              placeholder="POI's category"
                              variant='outlined'
                              disabled={true}
                              value={searchResultStore.poi !== undefined
                                && searchResultStore.poi.getCategory()} />
                            <FormHelperText>{categoryError && message}</FormHelperText>
                          </FormControl>
                        )
                                        }
                      <Button
                        variant={editedCategory === 'done' && !openEditCategory ? 'contained' : 'outlined'}
                        color={editedCategory === 'done' && !openEditCategory && 'primary'}
                        className={classes.formButton}
                        onClick={this.openEditCategory}>
                        {editedCategory === 'done' && !openEditCategory ? `${buttonText}됨` : `카테고리 ${buttonText}`}

                      </Button>
                      {openEditCategory && (
                      <Button
                        variant='outlined'
                        className={classes.formButton}
                        onClick={this.closeEditCategory}>
                        취소
                      </Button>
                      )}
                      {editedCategory === 'done' && !openEditCategory && (
                      <Button
                        variant='outlined'
                        className={classes.formButton}
                        onClick={this.resetCategory}>
                        초기화
                      </Button>
                      )
                                        }
                    </Grid>
                    <Grid item={true} xs={12}>
                      { openEditAddress ? <GeocodeSearch className={classes.editForm} isSearch={false} title='상세주소까지 입력하세요.' />
                        : (
                          <FormControl
                            required={true}
                            error={addressError} className={classes.longFormControl}>
                            <TextField
                              id='road-address'
                              label='도로명 주소(신주소) *'
                              placeholder="POI's road address"
                              variant='outlined'
                              disabled={true}
                              value={searchResultStore.poi !== undefined
                                && searchResultStore.poi.getRoadAddress()} />
                            <br />
                            <TextField
                              id='parcel-address'
                              label='지번 주소(구주소) *'
                              placeholder="POI's parcel address"
                              variant='outlined'
                              disabled={true}
                              value={searchResultStore.poi !== undefined
                                && searchResultStore.poi.getParcelAddress()} />
                            <FormHelperText>{addressError && message}</FormHelperText>
                          </FormControl>
                        )
                      }
                      <Button
                        variant={editedAddress === 'done' && !openEditAddress ? 'contained' : 'outlined'}
                        color={editedAddress === 'done' && !openEditAddress && 'primary'}
                        className={classes.formButton}
                        onClick={this.openEditAddress}>
                        {editedAddress === 'done' && !openEditAddress ? `${buttonText}됨` : `주소 ${buttonText}`}

                      </Button>
                      {openEditAddress && (
                      <Button
                        variant='outlined'
                        className={classes.formButton}
                        onClick={this.closeEditAddress}>
                        취소
                      </Button>
                      )}
                      {editedAddress === 'done' && !openEditAddress && (
                      <Button
                        variant='outlined'
                        className={classes.formButton}
                        onClick={this.resetAddress}>
                        초기화
                      </Button>
                      )
                      }
                    </Grid>
                    <Grid item={true} xs={12}>
                      <FormControl className={classes.formControl}>
                        <TextField
                          id='representation-phones'
                          label='대표전화'
                          placeholder="POI's Representation Phones"
                          variant='outlined'
                          defaultValue={poi.getRepresentationPhone()}
                          name='representationPhone'
                          onChange={this.handleText} />
                      </FormControl>
                      <FormControl className={classes.formControl}>
                        <TextField
                          id='normal-phones'
                          label='일반전화'
                          placeholder="POI's normal Phones"
                          variant='outlined'
                          defaultValue={poi.getNormalPhone()}
                          name='normalPhone'
                          onChange={this.handleText} />
                      </FormControl>
                      <FormControl className={classes.formControl}>
                        <TextField
                          id='fax'
                          label='팩스'
                          placeholder="POI's Fax"
                          variant='outlined'
                          defaultValue={poi.getFax()}
                          name='fax'
                          onChange={this.handleText} />
                      </FormControl>
                      <FormHelperText>
                        {' '}
                        {buttonText}
                        할 전화번호가 여러 개일 경우 , 를 구분자로 하여 넣으세요.
                      </FormHelperText>
                    </Grid>
                  </Grid>
                </form>
              )}
              { activeStep === 1 && (
                <Grid row={true} justify='center' className={classes.activePage}>
                  <Grid item={true} xs={12}>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originName !== searchResultStore.poi.name ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        명칭
                      </Typography>
                      <Typography variant='subtitle1'>
                        {poi.name}
                        {originName !== searchResultStore.poi.name && type === 'U'
                                                && (
                                                <span style={{ color: 'red' }}>
                                                  {' '}
                                                  <ArrowRight />
                                                  {' '}
                                                  {searchResultStore.poi.name}
                                                </span>
                                                )
                                            }
                      </Typography>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originBranch !== searchResultStore.poi.branch ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        브랜치
                      </Typography>
                      <Typography variant='subtitle1'>
                        {poi.branch}
                        {originBranch !== searchResultStore.poi.branch && type === 'U'
                                            && (
                                            <span style={{ color: 'red' }}>
                                              {' '}
                                              <ArrowRight />
                                              {' '}
                                              {searchResultStore.poi.branch}
                                            </span>
                                            )
                                            }
                      </Typography>
                    </FormControl>
                  </Grid>
                  <Grid item={true} xs={12}>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originCategory !== searchResultStore.poi.getCategory() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        카테고리
                      </Typography>
                      <Typography variant='subtitle1'>
                        {originCategory}
                        {originCategory !== searchResultStore.poi.getCategory() && type === 'I'
                                            && <span>{searchResultStore.poi.getCategory()}</span>
                                            }
                        {originCategory !== searchResultStore.poi.getCategory() && type === 'U'
                                            && (
                                            <span style={{ color: 'red' }}>
                                              {' '}
                                              <ArrowRight />
                                              {' '}
                                              {searchResultStore.poi.getCategory()}
                                            </span>
                                            )
                                            }
                      </Typography>
                    </FormControl>
                  </Grid>
                  <Grid item={true} xs={12}>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originRoadAddress !== searchResultStore.poi.getRoadAddress() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        도로명 주소(신주소)
                      </Typography>
                      <Typography variant='subtitle1'>
                        {originRoadAddress}
                        {originRoadAddress !== searchResultStore.poi.getRoadAddress() && type === 'I'
                                            && <span>{searchResultStore.poi.getRoadAddress()}</span>
                                            }
                        {originRoadAddress !== searchResultStore.poi.getRoadAddress() && type === 'U'
                                            && (
                                            <span style={{ color: 'red' }}>
                                              {' '}
                                              <ArrowRight />
                                              {' '}
                                              {searchResultStore.poi.getRoadAddress()}
                                            </span>
                                            )
                                            }
                      </Typography>
                    </FormControl>
                  </Grid>
                  <Grid item={true} xs={12}>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originParcelAddress !== searchResultStore.poi.getParcelAddress() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        지번 주소(구주소)
                      </Typography>

                      <Typography variant='subtitle1'>
                        {originParcelAddress}
                        {originParcelAddress !== searchResultStore.poi.getParcelAddress() && type === 'I'
                                          && <span>{searchResultStore.poi.getParcelAddress()}</span>
                                          }
                        {originParcelAddress !== searchResultStore.poi.getParcelAddress() && type === 'U'
                                            && (
                                            <span style={{ color: 'red' }}>
                                              {' '}
                                              <ArrowRight />
                                              {' '}
                                              {searchResultStore.poi.getParcelAddress()}
                                            </span>
                                            )
                                            }
                      </Typography>
                    </FormControl>
                  </Grid>
                  <Grid item={true} xs={12}>
                    <FormControl className={classes.formControl}>
                      <Typography
                        color={originalRepresentationPhone !== searchResultStore.poi.getRepresentationPhone() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        대표 전화번호
                      </Typography>
                      <Typography variant='subtitle1'>{poi.getRepresentationPhone()}</Typography>
                      <Typography
                        color={originalNormalPhone !== searchResultStore.poi.getNormalPhone() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        일반 전화번호
                      </Typography>
                      <Typography variant='subtitle1'>{poi.getNormalPhone()}</Typography>
                      <Typography
                        color={originalFax !== searchResultStore.poi.getFax() ? 'secondary' : 'primary'}
                        gutterBottom={true}>
                        팩스번호
                      </Typography>
                      <Typography variant='subtitle1'>{poi.getFax()}</Typography>
                    </FormControl>
                  </Grid>
                </Grid>
              )}
              { activeStep === 2 && searchResultStore.updated && (
                <Grid row={true} justify='center' className={classes.activePage}>
                  <Grid item={true} xs={12}>
                    {message}
                  </Grid>
                </Grid>
              )}
              <div className={classes.flexBar}>
                { activeStep !== 5 && (
                <Button
                  onClick={activeStep !== 0 ? this.handleBack : this.goToSearch}
                  className={classes.backButton}
                  size='large'>
                  {activeStep !== 0 ? '뒤로' : '닫기'}
                </Button>
                )}
                <Button
                  variant='contained'
                  color='primary'
                  onClick={activeStep !== 2 ? this.handleNext : this.goToSearch}
                  size='large'>
                  {this.stepActions()}
                </Button>
              </div>
            </div>
          </Grid>
        </Grid>
      )
    }
}

Editor.propTypes = {
  classes: PropTypes.object.isRequired,
  poi: PropTypes.object,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
  type: PropTypes.object,
}

export default withStyles(styles)(Editor)
