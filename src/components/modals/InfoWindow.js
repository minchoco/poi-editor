import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Card, CardContent, Typography } from '@material-ui/core'

const oldAddressImg = require('../../images/old_address.png')

const styles = {
  card: {
    minWidth: 275,
  },
  cardContent: {
    textAlign: 'left',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  oldAddress: {
    display: 'flex',
  },
  oldAddressText: {
    paddingTop: '2px',
    paddingLeft: '2px',
  },
}

const InfoWindow = ({ classes, poi }) => (
  <Card className={classes.card}>
    <CardContent className={classes.cardContent}>
      <Typography variant='h5' component='h2'>
        {poi.getName()}
      </Typography>
      <Typography className={classes.pos} color='textSecondary'>
        {poi.getCategory()}
      </Typography>
      <Typography component='p'>
        {poi.getRoadAddress()}
      </Typography>
      <div className={classes.oldAddress}>
        <Typography component='span'>
          <img width={33} src={oldAddressImg} alt='oldAddress' />
        </Typography>
        <Typography component='span' className={classes.oldAddressText}>
          {poi.getParcelAddress()}
        </Typography>
      </div>
      {poi.getRepresentationPhone() !== undefined
          && (
          <Typography component='p'>
              대표전화 :
            {' '}
            {poi.getRepresentationPhone()}
          </Typography>
          ) }
      {poi.getNormalPhone() !== undefined
          && (
          <Typography component='p'>
              일반전화 :
            {' '}
            {poi.getNormalPhone()}
          </Typography>
          ) }
      {poi.getFax() !== undefined
          && (
          <Typography component='p'>
              팩스 :
            {' '}
            {poi.getFax()}
          </Typography>
          ) }
      {poi.getDescription() !== undefined
          && (
          <Typography component='p'>
            {poi.getDescription()}
          </Typography>
          ) }
      {poi.getHomePageURL() !== undefined
          && (
          <Typography component='p'>
              홈페이지 :
            {' '}
            {poi.getHomePageURL()}
          </Typography>
          ) }
      {poi.getOpeningHours() !== undefined
          && (
          <Typography component='p'>
              영업시간 :
            {' '}
            {poi.getOpeningHours()}
          </Typography>
          ) }
      {poi.getHours24() !== undefined && poi.getHours24()
          && (
          <Typography component='p'>
              24시간 영업
          </Typography>
          ) }
      {poi.getParkingService() !== undefined && poi.getParkingService()
          && (
          <Typography component='p'>
              주차가능
          </Typography>
          ) }
      {poi.getParkingType() !== undefined
          && (
          <Typography component='p'>
              주차형태 :
            {' '}
            {poi.getParkingType()}
          </Typography>
          ) }
      {poi.getParkingPrice() !== undefined
          && (
          <Typography component='p'>
              주차비 :
            {' '}
            {poi.getParkingPrice()}
          </Typography>
          ) }
      {poi.getCreditcardService() !== undefined && poi.getCreditcardService()
          && (
          <Typography component='p'>
              신용카드 사용 가능
          </Typography>
          ) }
      {poi.getPetService() !== undefined && poi.getPetService()
          && (
          <Typography component='p'>
              애완견 동반 가능
          </Typography>
          ) }
      {poi.getStrollerRental() !== undefined && poi.getStrollerRental()
          && (
          <Typography component='p'>
              유모차 대여 가능
          </Typography>
          ) }

    </CardContent>
  </Card>
)

InfoWindow.propTypes = {
  classes: PropTypes.object.isRequired,
  poi: PropTypes.object,
}

export default withStyles(styles)(InfoWindow)
