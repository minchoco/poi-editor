import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import { Button, Typography } from '@material-ui/core'

function rand() {
  return Math.round(Math.random() * 20) - 10
}

function getModalStyle() {
  const top = 30 + rand()
  const left = 50 + rand()
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  }
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  buttonGroup: {
    padding: '10px',
    width: '100%',
  },
  button: {
    width: '50%',
  },
})

@inject('searchStore', 'searchResultStore')
@observer
class Deletor extends Component {
    state= {
      message: '',
    }

    handleDelete = () => {
      const { searchStore, searchResultStore, poi } = this.props
      const id = poi.getId()
      searchStore.delete(id, () => {
        if (searchResultStore.updated) {
          this.goToSearch()
          searchStore.search()
        } else {
          this.setState({ message: '문제가 발생했습니다.' })
        }
      })
    }

    goToSearch = () => {
      const { searchResultStore } = this.props
      searchResultStore.deleteModalOpen = false
    }

    render() {
      const { classes, poi } = this.props
      const { message } = this.state

      return (
        <div style={getModalStyle()} className={classes.paper}>
          <Typography variant='h6' id='modal-title'>
            <p>
              {' '}
              {poi.getName()}
              {' '}
            </p>
            정말로 삭제하시겠습니까?
          </Typography>
          <div className={classes.buttonGroup}>
            <Button
              variant='contained' color='primary' className={classes.button}
              onClick={this.handleDelete}>
              네 확신합니다.
            </Button>
            <Button variant='outlined' className={classes.button} onClick={this.goToSearch}>취소</Button>
          </div>
          {message}
        </div>
      )
    }
}

Deletor.propTypes = {
  classes: PropTypes.object,
  poi: PropTypes.object,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(Deletor)
