import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { observer, inject } from 'mobx-react'
import withStyles from '@material-ui/core/styles/withStyles'
import PropTypes from 'prop-types'
import {
  Button, TextField, Typography, Grid, Paper,
} from '@material-ui/core'

const styles = theme => ({
  errorMessage: {
    marginTop: 10,
    marginBottom: 10,
  },
  root: {
    padding: theme.spacing.unit * 20,
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
  paper: {
    padding: '30px',
  },
  button: {
    margin: '0 5px 10px',
  },
})

@inject('managementStore')
@observer
class Login extends Component {
  state = {
    key: '',
    server: 'b2c',
    env: 'prd',
  }

  handleChange = (e, target, nextValue) => {
    const { managementStore } = this.props
    if (nextValue === '') {
      // eslint-disable-next-line no-param-reassign
      nextValue = e.target.value
    }
    this.setState({ [target]: nextValue })
    if (target === 'server') {
      managementStore.serverZone = nextValue
    } else if (target === 'env') {
      managementStore.setEnvironment(nextValue)
    }
  }

  render() {
    const { classes, managementStore } = this.props
    const { key, server, env } = this.state

    return (
      <Grid
        container={true} spacing={24} alignItems='center'
        justify='center' className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container={true}>
            <Grid item={true} xs={12}>
              <Grid container={true}>
                <Grid item={true} xs={1}>서버</Grid>
                <Grid item={true} xs={11}>
                  <Button
                    className={classes.button}
                    variant={server === 'b2c' ? 'contained' : 'outlined'}
                    color={server === 'b2c' ? 'primary' : undefined}
                    onClick={event => this.handleChange(event, 'server', 'b2c')}
                    value='b2c'>
                    B2C
                  </Button>
                  <Button
                    className={classes.button}
                    variant={server === 'b2b' ? 'contained' : 'outlined'}
                    color={server === 'b2b' ? 'primary' : undefined}
                    onClick={event => this.handleChange(event, 'server', 'b2b')}
                    value='b2b'>
                    B2B
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item={true} xs={12}>
              <Grid container={true}>
                <Grid item={true} xs={1}>환경</Grid>
                <Grid item={true} xs={11}>
                  <Button
                    className={classes.button}
                    variant={env === 'prd' ? 'contained' : 'outlined'}
                    color={env === 'prd' ? 'primary' : undefined}
                    onClick={event => this.handleChange(event, 'env', 'prd')}
                    value='prd'
                    disabled={server === 'b2b'}>
                    Production
                  </Button>
                  <Button
                    className={classes.button}
                    variant={env === 'tb' ? 'contained' : 'outlined'}
                    color={env === 'tb' ? 'primary' : undefined}
                    onClick={event => this.handleChange(event, 'env', 'tb')}
                    value='tb'>
                    TestBed
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item={true} xs={12}>
              <TextField
                id='authorization-key'
                label='Key'
                helperText='eg. Bearer ...'
                variant='outlined'
                fullWidth={true}
                name='key'
                onChange={event => this.handleChange(event, 'key', '')}
                onKeyPress={(e) => { if (e.key === 'Enter') managementStore.checkKey(e.target.value, server, env) }} />
            </Grid>
            <Grid item={true} xs={12}>
              <Typography color='error' variant='caption' className={classes.errorMessage}>{managementStore.loginStatusMessage}</Typography>
            </Grid>
            <Grid item={true} xs={12}>
              <Button
                fullWidth={true} className={classes.button} variant='contained'
                onClick={() => managementStore.checkKey(key, server, env)} color='primary' autoFocus={true}>
                Getting started
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  managementStore: PropTypes.object,
}

export default withRouter(withStyles(styles)(Login))
