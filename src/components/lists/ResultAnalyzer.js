import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = () => ({
  root: {
    padding: 25,
    width: '600px',
  },
})

@inject('searchResultStore', 'mapElementStore')
@observer
class ResultAnalyzer extends Component {
  handleClick = () => {
    const { searchResultStore } = this.props
    searchResultStore.anchorEl.analyzer = null
  };

  render() {
    const { classes, searchResultStore } = this.props
    return (
      <div className={classes.root}>
        <Typography component='h6' variant='h6' gutterBottom={true}>
          장소 관점 분석 결과
        </Typography>
        {searchResultStore.analyzedResult !== undefined
            && (
            <div>
              <Typography component='body2' variant='body2'>
                {' - 패턴 : '}
                { searchResultStore.analyzedResult.getPoiTermType()}
              </Typography>
              <Typography component='body2' variant='body2'>
                {' - 토큰 : '}
                {searchResultStore.analyzedResult.getPoiTokens()}
              </Typography>
            </div>
            )}
        <br />
        <Typography component='h6' variant='h6' gutterBottom={true}>
          주소 관점 분석 결과
        </Typography>
        {searchResultStore.analyzedResult !== undefined
            && (
            <div>
              <Typography component='body2' variant='body2'>
                {' - 패턴 : '}
                {searchResultStore.analyzedResult.getAddressTermType()}
              </Typography>
              <Typography component='body2' variant='body2'>
                {' - 토큰 : '}
                {searchResultStore.analyzedResult.getAddressTokens()}
              </Typography>
            </div>
            )}
      </div>
    )
  }
}

ResultAnalyzer.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResultStore: PropTypes.object,
}

export default withStyles(styles)(ResultAnalyzer)
