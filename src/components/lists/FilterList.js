import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import {
  Grid, FormControlLabel, Switch, FormControl, Checkbox, Button, TextField,
} from '@material-ui/core'

const styles = () => ({
  grid: {
    padding: 25,
    width: '400px',
  },
  buttonGroup: {
    padding: '10px',
    maxHeight: 50,
    botton: 0,
  },
})

@inject('searchStore', 'searchResultStore')
@observer
class FilterList extends Component {
    state = {
      checkedB2C: true,
    };

    componentDidMount() {
      const { searchResultStore } = this.props
      this.setState({ checkedB2C: searchResultStore.selectedB2CFilters })
    }

    handleChange = name => (event) => {
      const { searchStore, searchResultStore } = this.props
      let value = event.target.checked
      if (name === 'mode') {
        if (value) { value = 'NAVIGATION' } else { value = 'ALL' }
      } else if (name === 'children') {
        if (value) { value = 'ON' } else { value = 'OFF' }
      }
      if (name === 'checkedB2C') {
        if (event.target.checked) {
          searchStore.setOptions('mode', 'NAVIGATION')
          searchStore.setOptions('categoryOnly', true)
          searchStore.setOptions('autoCorrection', true)
          searchStore.setOptions('children', 'OFF')
          searchStore.setOptions('includePOISearchResultsWithDetailAddressSearch', false)
          searchStore.setOptions('includePOISearchResultsWithLimitedAddressSearch', false)
        } else {
          searchStore.setOptions('mode', 'ALL')
          searchStore.setOptions('categoryOnly', false)
          searchStore.setOptions('autoCorrection', false)
          searchStore.setOptions('children', 'ON')
          searchStore.setOptions('includePOISearchResultsWithDetailAddressSearch', true)
          searchStore.setOptions('includePOISearchResultsWithLimitedAddressSearch', true)
        }
        this.setState({ checkedB2C: event.target.checked })
      } else {
        searchStore.setOptions(name, value)
        searchResultStore.selectedB2CFilters = false
        this.setState({ checkedB2C: false })
      }
    };

    handleSearchClick = () => {
      const { searchStore } = this.props
      searchStore.search()
    };

    handleSubmitClick = () => {
      const { searchStore, searchResultStore } = this.props
      searchStore.search(() => {
        searchResultStore.anchorEl.filter = null
      })
    }

    render() {
      const { classes, searchStore, searchResultStore } = this.props
      const { checkedB2C } = this.state
      const { integratedOptions, dataOptions } = searchStore

      return (
        <Grid container={true} alignItems='stretch' className={classes.grid}>
          {searchResultStore.searchMode === 'integrated'
            ? (
              <FormControl className={classes.selectBox} fullWidth={true}>
                <Grid item={true} md={12}>
                  <FormControlLabel
                    control={(
                      <Checkbox
                        checked={checkedB2C}
                        onChange={this.handleChange('checkedB2C')}
                        value='checkedB2C'
                        color='secondary' />
)}
                    label='B2C default option' />
                </Grid>
                <Grid item={true} xs={12} style={{ padding: 0 }}>
                  <Grid container={true}>
                    <Grid item={true} xs={6} md={6}>
                      <FormControlLabel
                        control={(
                          <Switch
                            checked={integratedOptions.mode === 'NAVIGATION'}
                            onChange={this.handleChange('mode')}
                            value='mode'
                            color='primary'
                            name='mode' />
)}
                        label='네비게이션 모드' />
                      <FormControlLabel
                        control={(
                          <Switch
                            checked={integratedOptions.categoryOnly}
                            onChange={this.handleChange('categoryOnly')}
                            value='categoryOnly'
                            color='primary'
                            name='categoryOnly' />
)}
                        label='Name 확장' />
                    </Grid>
                    <Grid item={true} xs={6} md={6}>
                      <FormControlLabel
                        control={(
                          <Switch
                            checked={integratedOptions.autoCorrection}
                            onChange={this.handleChange('autoCorrection')}
                            value='autoCorrection'
                            color='primary'
                            name='autoCorrection' />
)}
                        label='오타보정' />
                      <FormControlLabel
                        control={(
                          <Switch
                            checked={integratedOptions.children === 'ON'}
                            onChange={this.handleChange('children')}
                            value='children'
                            color='primary'
                            name='children' />
)}
                        label='자식 확장' />
                    </Grid>
                    <Grid item={true} xs={12}>
                      <FormControlLabel
                        control={(
                          <Switch
                            // eslint-disable-next-line max-len
                            checked={integratedOptions.includePOISearchResultsWithDetailAddressSearch}
                            onChange={this.handleChange('includePOISearchResultsWithDetailAddressSearch')}
                            value='includePOISearchResultsWithDetailAddressSearch'
                            color='primary'
                            name='includePOISearchResultsWithDetailAddressSearch' />
)}
                        label='상세 주소 POI 검색' />
                    </Grid>
                    <Grid item={true} xs={12}>
                      <FormControlLabel
                        control={(
                          <Switch
                            // eslint-disable-next-line max-len
                            checked={integratedOptions.includePOISearchResultsWithLimitedAddressSearch}
                            onChange={this.handleChange('includePOISearchResultsWithLimitedAddressSearch')}
                            value='includePOISearchResultsWithLimitedAddressSearch'
                            color='primary'
                            name='includePOISearchResultsWithLimitedAddressSearch' />
)}
                        label='비상세 주소 POI 검색' />
                    </Grid>
                  </Grid>
                </Grid>
              </FormControl>
            )
            : (
              <FormControl className={classes.selectBox} fullWidth={true}>
                <Grid item={true} xs={12}>
                  <TextField
                    id='branch'
                    label='Branch'
                    placeholder="POI's Branch"
                    value={dataOptions.branch || ''}
                    onChange={this.handleChange('branch')} />
                </Grid>
                <Grid item={true} xs={12}>
                  <TextField
                    id='poi-id'
                    label='ID'
                    fullWidth={true}
                    placeholder='POI ID'
                    value={dataOptions.id || ''}
                    onChange={this.handleChange('id')} />
                </Grid>
                <Grid item={true} xs={12}>
                  <TextField
                    id='poi-ref-id'
                    label='Original ID'
                    fullWidth={true}
                    placeholder='POI Original ID'
                    value={dataOptions.poiId || ''}
                    onChange={this.handleChange('poiId')} />
                </Grid>
              </FormControl>
            )
                }

          <div className={classes.buttonGroup}>
            <Button
              fullWidth={true}
              variant='contained' color='primary'
              onClick={event => this.handleSubmitClick(event)}>
                        적용
            </Button>
          </div>
        </Grid>
      )
    }
}

FilterList.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(FilterList)
