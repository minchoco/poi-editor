import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import {
  FormGroup, List, ListItem, ListItemText, Button, Typography, CircularProgress, ListSubheader,
} from '@material-ui/core'

const styles = () => ({
  formGroup: {
    maxHeight: 400,
    overflowY: 'hidden',
    display: 'inline-flex',
    padding: '10px',
  },
  menu: {
    fontSize: '0.8rem',
  },
  categoryList: {
    maxHeight: 400,
    overflowY: 'auto',
  },
  select: {
    fontSize: '0.8rem',
    paddingRight: 0,
  },
  textField: {
    paddingRight: 10,
  },
  buttonGroup: {
    padding: '10px',
    maxHeight: 50,
  },
  item: {
    paddingTop: '0px',
  },
  progress: {
    margin: '20px',
  },
  notPrevSelected: {
    fontSize: '0.2rem',
    marginTop: '80px',
    textAlign: 'center',
  },
  cancleBtn: {
    width: '50%',
  },
  submitBtn: {
    width: '50%',
  },
})

@inject('searchStore', 'searchResultStore')
@observer
class CategoryList extends Component {
  componentDidMount() {
    const { searchStore } = this.props
    searchStore.getCategoryList('master', '')
  }

  isSelected = (name, code) => {
    const { searchResultStore } = this.props
    return searchResultStore.selectedCategory[name].code === code
  }

  handleClick = (event, current, target, name, code, isSearch) => {
    const { searchStore, searchResultStore } = this.props
    const value = { code, name }
    searchResultStore.selectedCategory[current] = value

    if (isSearch) {
      if (current === 'master' && code === 'all') {
        searchStore.setOptions('category.code', undefined)
        searchResultStore.categoryList.middle = []
        searchResultStore.categoryList.sub = []
      } else {
        if (target !== '') {
          searchStore.getCategoryList(target, code)
        }
        searchStore.setOptions('category.code', code)
      }
      const defaultValue = { code: 'all', name: 'all' }
      if (current === 'master') {
        if (!searchResultStore.selectedCategory.middle.code.includes(code)
                  && searchResultStore.selectedCategory.middle.code !== 'all') {
          searchResultStore.selectedCategory.middle = defaultValue
          searchResultStore.selectedCategory.sub = defaultValue
        }
      } else if (current === 'middle') {
        if (!searchResultStore.selectedCategory.sub.code.includes(code)
                  && searchResultStore.selectedCategory.sub.code !== 'all') {
          searchResultStore.selectedCategory.sub = defaultValue
        }
      }
    } else {
      if (target !== '') {
        searchStore.getCategoryList(target, code)
      }
      if (current === 'sub' && searchResultStore.poi !== undefined) {
        const category = searchResultStore.selectedCategory
        searchResultStore.poi.newCategory = {
          masterCode: category.master.code,
          masterName: category.master.name,
          middleCode: category.middle.code,
          middleName: category.middle.name,
          subCode: category.sub.code,
          subName: category.sub.name,
        }
      }
    }
  };

  handleResetClick = () => {
    const { searchStore, searchResultStore } = this.props
    searchResultStore.resetSelectedCategory()
    searchStore.setOptions('category.code', undefined)
    searchStore.search(() => {
      searchResultStore.anchorEl.category = null
    })
  }

  handleSubmitClick = () => {
    const { searchStore, searchResultStore } = this.props
    searchStore.search(() => {
      searchResultStore.anchorEl.category = null
    })
  }

  render() {
    const { classes, searchResultStore, isSearch } = this.props

    return (
      <React.Fragment>
        <FormGroup row={true} className={classes.formGroup}>
          <List id='category-master' className={classes.categoryList}>
            <ListSubheader
              className={classes.item}
              key='all'
              button={true}
              selected={this.isSelected('master', 'all')}
              onClick={event => this.handleClick(event, 'master', '', 'all', 'all', isSearch)}>
              <Typography variant='body2'>대분류 전체</Typography>
            </ListSubheader>
            {searchResultStore.getStep.first
                          && <CircularProgress className={classes.progress} />
                      }
            {searchResultStore.categoryList.master.map(item => (
              <ListItem
                className={classes.item}
                key={item.code}
                button={true}
                selected={this.isSelected('master', item.code)}
                onClick={event => this.handleClick(event, 'master', 'middle', item.name, item.code, isSearch)}>
                <ListItemText><Typography variant='body2'>{item.name}</Typography></ListItemText>
              </ListItem>
            ))}
          </List>
          <List id='category-middle' className={classes.categoryList}>
            <ListSubheader
              className={classes.item}
              key='all'
              button={true}
              selected={this.isSelected('middle', 'all')}
              onClick={event => this.handleClick(event, 'middle', '', 'all', 'all', isSearch)}>
              <Typography variant='body2'>중분류 전체</Typography>
            </ListSubheader>
            {searchResultStore.getStep.second
              ? <CircularProgress className={classes.progress} />
              : searchResultStore.categoryList.middle.length === 0
                                  && (
                                  <div className={classes.notPrevSelected}>
                                    선택된 대분류가
                                    {' '}
                                    <br />
                                    없습니다.
                                  </div>
                                  )
                      }
            {searchResultStore.categoryList.middle.map(item => (
              <ListItem
                className={classes.item}
                key={item.code}
                button={true}
                selected={this.isSelected('middle', item.code)}
                onClick={event => this.handleClick(event, 'middle', 'sub', item.name, item.code, isSearch)}>
                <ListItemText><Typography variant='body2'>{item.name}</Typography></ListItemText>
              </ListItem>
            ))}
          </List>
          <List id='category-sub' className={classes.categoryList}>
            <ListSubheader
              className={classes.item}
              key='all'
              button={true}
              selected={this.isSelected('sub', 'all')}
              onClick={event => this.handleClick(event, 'sub', '', 'all', 'all', isSearch)}>
              <Typography variant='body2'>소분류 전체</Typography>
            </ListSubheader>
            {searchResultStore.getStep.third
              ? <CircularProgress className={classes.progress} />
              : searchResultStore.categoryList.sub.length === 0
                                  && (
                                  <div className={classes.notPrevSelected}>
                                    선택된 중분류가
                                    {' '}
                                    <br />
                                    없습니다.
                                  </div>
                                  )
                      }
            {searchResultStore.categoryList.sub.map(item => (
              <ListItem
                className={classes.item}
                key={item.code}
                button={true}
                selected={this.isSelected('sub', item.code)}
                onClick={event => this.handleClick(event, 'sub', '', item.name, item.code, isSearch)}>
                <ListItemText><Typography variant='body2'>{item.name}</Typography></ListItemText>
              </ListItem>
            ))}
          </List>
        </FormGroup>
        {isSearch && (
        <div className={classes.buttonGroup}>
          <Button
            fullWidth={true}
            variant='contained'
            className={classes.cancleBtn}
            onClick={event => this.handleResetClick(event)}>
                      삭제
          </Button>
          <Button
            fullWidth={true}
            variant='contained' color='primary'
            className={classes.submitBtn}
            onClick={event => this.handleSubmitClick(event, isSearch)}>
                      적용
          </Button>
        </div>
        )}
      </React.Fragment>
    )
  }
}

CategoryList.propTypes = {
  classes: PropTypes.object.isRequired,
  isSearch: PropTypes.bool,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(CategoryList)
