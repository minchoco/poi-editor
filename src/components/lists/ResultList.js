import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Button, Typography } from '@material-ui/core'

const styles = () => ({
  root: {
    padding: 25,
    width: '300px',
  },
  buttonGroup: {
    botton: 0,
  },
  poiBtn: {
    width: '50%',
  },
  addressBtn: {
    width: '50%',
  },
})

@inject('searchResultStore', 'mapElementStore')
@observer
class ResultList extends Component {
    handleClick = (event, type) => {
      const { searchResultStore, mapElementStore } = this.props
      if (type === 'poi') {
        mapElementStore.handleResultList(mapElementStore.resultPoiMarkerList, 'show')
        mapElementStore.handleResultList(mapElementStore.resultAddressMarkerList, 'hide')
        mapElementStore.handleResultList(mapElementStore.resultAddressPolygonList, 'hide')
      } else {
        mapElementStore.handleResultList(mapElementStore.resultPoiMarkerList, 'hide')
        mapElementStore.handleResultList(mapElementStore.resultAddressMarkerList, 'show')
        mapElementStore.handleResultList(mapElementStore.resultAddressPolygonList, 'show')
      }
      searchResultStore.resultMode = type
      searchResultStore.anchorEl.result = null
    };

    render() {
      const { classes, searchResultStore } = this.props

      return (
        <div className={classes.root}>
          <Typography variant='body2'>
                    장소 결과:
            {' '}
            {searchResultStore.numberOfPois}
          </Typography>
          <Typography variant='body2'>
                    주소 결과 :
            {' '}
            {searchResultStore.numberOfAddress}
          </Typography>
          <div className={classes.buttonGroup}>
            <Button
              className={classes.poiBtn}
              variant={searchResultStore.resultMode === 'poi' ? 'contained' : 'outlined'}
              color={searchResultStore.resultMode === 'poi' && 'primary'}
              onClick={event => this.handleClick(event, 'poi')}
              disabled={searchResultStore.numberOfPois === 0}>
                        장소 검색 결과
            </Button>
            <Button
              className={classes.addressBtn}
              variant={searchResultStore.resultMode === 'address' ? 'contained' : 'outlined'}
              color={searchResultStore.resultMode === 'address' && 'primary'}
              onClick={event => this.handleClick(event, 'address')}
              disabled={searchResultStore.numberOfAddress === 0}>
                        주소 검색 결과
            </Button>
          </div>
        </div>
      )
    }
}

ResultList.propTypes = {
  classes: PropTypes.object.isRequired,
  mapElementStore: PropTypes.object,
  searchResultStore: PropTypes.object,
}

export default withStyles(styles)(ResultList)
