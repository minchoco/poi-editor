import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import {
  Typography, Table, TableBody, TableCell, TableFooter, TablePagination,
  TableRow, Paper, CircularProgress,
} from '@material-ui/core'
import LocationOn from '@material-ui/icons/LocationOn'
import ButtonBar from '../buttons/ButtonBar'
import TablePaginationActionsWrapped from '../common/TablePaginationActionsWrapped'
import PoiResult from '../../models/PoiResult'

const oldAddressImg = require('../../images/old_address.png')

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto',
    backgroundSize: 'cover',
    backgroundPosition: '0 400px',
    textAlign: 'center',
  },
  table: {
    height: '100%',
  },
  tableRow: {
    overflowY: 'scroll',
  },
  tableWrapper: {
    overflowX: 'hidden',
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  tableRowTh: {
    padding: '8px 5px 10px 10px',
  },
  tableItem: {
    display: 'inline-block',
  },
  inline: {
    display: 'inline',
  },
  locationOn: {
    paddingLeft: 10,
  },
  poiName: {
    fontWeight: 'bold',
    lineHeight: '2.00',
  },
  progress: {
    marginTop: '20px',
  },
  oldAddress: {
    display: 'flex',
    overflowY: 'auto',
  },
  oldAddressText: {
    paddingTop: '2px',
    paddingLeft: '2px',
    maxHeight: '100px',
  },
  pagination: {
    textAlign: 'center',
  },
})

@inject('searchStore', 'searchResultStore', 'mapElementStore', 'managementStore')
@observer
class SearchResultList extends Component {
    state = {
      rowsPerPage: 10,
    };

    handleChangePage = (event, page) => {
      const { searchResultStore, searchStore } = this.props
      const { rowsPerPage } = this.state

      if (searchResultStore.resultMode === 'poi') {
        searchStore.setOptions('start', page * rowsPerPage)
      } else {
        searchStore.setOptions('startOfAddress', page * rowsPerPage)
      }
      searchStore.search()
      searchResultStore.showPage = page
      window.scrollTo(0, 0)
    };

    handleClick = (event, id, index) => {
      const { searchStore, mapElementStore } = this.props
      if (id !== '') {
        if (mapElementStore.selectedId === id) {
          return
        }
        mapElementStore.selectedId = (id !== '' ? id : index)
        searchStore.get(id).then((response) => {
          if (response.status === 200) {
            const result = new PoiResult(response.data.pois[0])
            mapElementStore.setWindow(event, index, result)
          }
        })
      } else {
        mapElementStore.fitMap('polygon', [mapElementStore.resultAddressPolygonList[index]])
      }
    }

    isSelected = (id) => {
      const { mapElementStore } = this.props
      if (id === mapElementStore.selectedId) {
        return true
      }
      return false
    }

    render() {
      const { classes, searchResultStore, managementStore } = this.props
      const { rowsPerPage } = this.state

      return (
        <Paper className={classes.root}>
          {searchResultStore.searching
              && <CircularProgress className={classes.progress} />
            }
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
              <TableBody className={classes.tableRow}>
                {searchResultStore.resultMode === 'poi' && searchResultStore.result.poiList !== undefined && searchResultStore.result.poiList.map((poi, index) => (
                  <TableRow
                    key={poi.getId()}
                    onClick={event => this.handleClick(event, poi.getId(), index)}
                    selected={this.isSelected(poi.getId())}>
                    <TableCell padding='none' className={classes.locationOn}>
                      <LocationOn />
                    </TableCell>
                    <TableCell component='th' scope='row' className={classes.tableRowTh}>
                      <React.Fragment>
                        <Typography variant='button' className={classes.poiName}>
                          {poi.getName()}
                        </Typography>
                        <Typography variant='body2' className={classes.inline}>
                          {poi.getRoadAddress()}
                        </Typography>
                        <div className={classes.oldAddress}>
                          <Typography component='span'>
                            <img width={33} src={oldAddressImg} alt='oldAddress' />
                          </Typography>
                          <Typography component='span' className={classes.oldAddressText}>
                            {poi.getSubParcelAddress()}
                          </Typography>
                        </div>
                        <Typography variant='caption'>
                          {poi.getCategory()}
                          {' '}
                          {poi.getDistance()}
                        </Typography>
                      </React.Fragment>
                    </TableCell>
                    {(managementStore.admin || managementStore.editor)
                    && (
                    <TableCell padding='none'>
                      <ButtonBar poi={poi} />
                    </TableCell>
                    )}
                  </TableRow>
                ))}
                {searchResultStore.resultMode === 'address' && searchResultStore.result.addressList.map((address, index) => (
                  <TableRow onClick={event => this.handleClick(event, '', index)} selected={this.isSelected(index)}>
                    <TableCell padding='none' className={classes.locationOn}>
                      <LocationOn />
                    </TableCell>
                    <TableCell component='th' scope='row' className={classes.tableRowTh}>
                      <Typography variant='body2' className={classes.inline}>
                        {address.roadAddress.map(roadAddr => (
                          roadAddr.fullAddress
                        ))}
                      </Typography>
                      {address.parcelAddress.length !== 0
                          && (
                          <div className={classes.oldAddress}>
                            <Typography component='span'>
                              <img width={33} src={oldAddressImg} alt='oldAddress' />
                            </Typography>
                            <Typography component='span' className={classes.oldAddressText}>
                              {address.parcelAddress.map(parcelAddr => (
                                <div>{parcelAddr.fullAddress}</div>
                              ))}
                            </Typography>
                          </div>
                          )
                        }
                    </TableCell>
                  </TableRow>
                ))}
                {!searchResultStore.searching && searchResultStore.searchOnce
                      && (searchResultStore.resultMode === 'poi' ? searchResultStore.numberOfPois === 0 : searchResultStore.numberOfAddress === 0)
                    && (
                    <TableRow>
                      <TableCell component='th'>검색결과가 없습니다.</TableCell>
                    </TableRow>
                    )
                  }
              </TableBody>
              {searchResultStore.searchOnce
                  && (
                  <TableFooter>
                    <TableRow>
                      <TablePagination
                        className={classes.pagination}
                        rowsPerPageOptions={[]}
                        colSpan={3}
                        count={searchResultStore.resultMode === 'poi' ? searchResultStore.numberOfPois : searchResultStore.numberOfAddress}
                        rowsPerPage={rowsPerPage}
                        page={searchResultStore.showPage}
                        SelectProps={{
                          native: true,
                        }}
                        onChangePage={this.handleChangePage}
                        ActionsComponent={TablePaginationActionsWrapped} />
                    </TableRow>
                  </TableFooter>
                  )
                }
            </Table>
          </div>
        </Paper>
      )
    }
}

SearchResultList.propTypes = {
  classes: PropTypes.object.isRequired,
  managementStore: PropTypes.object,
  mapElementStore: PropTypes.object,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(SearchResultList)
