import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import {
  FormGroup, List, ListItem, ListItemText, Button, Typography, CircularProgress,
  Tabs, Tab, AppBar, ListSubheader,
} from '@material-ui/core'
import SwipeableViews from 'react-swipeable-views'
import GeocodeSearch from '../GeocodeSearch'

function TabContainer({ children, dir }) {
  return (
    <Typography component='div' dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  )
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
}

const styles = () => ({
  root: {
    overflow: 'none',
    maxHeight: 600,
  },
  formGroup: {
    maxHeight: 400,
    overflowY: 'hidden',
  },
  menu: {
    fontSize: '0.8rem',
  },
  select: {
    fontSize: '0.8rem',
    paddingRight: 0,
  },
  textField: {
    paddingRight: 10,
    width: '50%',
  },
  buttonGroup: {
    padding: '10px',
    maxHeight: 50,
    botton: 0,
  },
  item: {
    paddingTop: '0px',
    backgroundColor: 'white',
  },
  progress: {
    margin: '20px',
  },
  notPrevSelected: {
    fontSize: '0.2rem',
    marginTop: '80px',
    textAlign: 'center',
  },
  addressList: {
    maxHeight: 400,
    overflowY: 'auto',
    width: '33%',
  },
  cancleBtn: {
    width: '50%',
  },
  submitBtn: {
    width: '50%',
  },
})

@inject('searchStore', 'searchResultStore')
@observer
class AddressList extends Component {
    state = {
      tabValue: 0,
    };

    componentDidMount() {
      const { searchStore } = this.props
      searchStore.getAddressList()
    }

    handleChange = (event, value) => {
      this.setState({ tabValue: value })
    }

    handleChangeIndex = (index) => {
      this.setState({ tabValue: index })
    }

    handleClick = (event, current, target, name, code) => {
      const { searchStore, searchResultStore } = this.props
      searchResultStore.addressPath = ''

      if (current === 'siDo' && code === 'all') {
        searchStore.setOptions('address.code', undefined)
        searchResultStore.addressList.setAddressList('siGunGu', [])
        searchResultStore.addressList.setAddressList('eupMyeonDong', [])
      } else {
        if (target !== '') {
          searchStore.getAddressList(current, code)
        }
        searchStore.setOptions('address.code', code)
      }
      const value = { code, name }
      searchResultStore.selectedAddress[current] = value
      const defaultValue = { code: 'all', name: 'all' }
      if (current === 'siDo') {
        if (!searchResultStore.selectedAddress.siGunGu.code.includes(code)
                && searchResultStore.selectedAddress.siGunGu.code !== 'all') {
          searchResultStore.selectedAddress.siGunGu = defaultValue
          searchResultStore.selectedAddress.eupMyeonDong = defaultValue
        }
      } else if (current === 'siGunGu') {
        if (!searchResultStore.selectedAddress.eupMyeonDong.code.includes(code)
                && searchResultStore.selectedAddress.eupMyeonDong.code !== 'all') {
          searchResultStore.selectedAddress.eupMyeonDong = defaultValue
        }
      }
    };

    handleResetClick = () => {
      const { searchStore, searchResultStore } = this.props
      searchResultStore.resetSelectedAddress()
      searchStore.setOptions('address.code', undefined)
      searchStore.search(() => {
        searchResultStore.anchorEl.address = null
      })
    }

    handleSubmitClick = () => {
      const { searchStore, searchResultStore } = this.props
      searchStore.search(() => {
        searchResultStore.anchorEl.address = null
      })
    }

    isSelected = (name, code) => {
      const { searchResultStore } = this.props
      return searchResultStore.selectedAddress[name].code === code
    }

    render() {
      const { classes, searchResultStore, theme } = this.props
      const { tabValue } = this.state

      return (
        <div className={classes.root}>
          <AppBar position='static' color='default'>
            <Tabs
              value={tabValue}
              onChange={this.handleChange}
              indicatorColor='primary'
              textColor='primary'
              variant='fullWidth'>
              <Tab label='주소 검색' />
              <Tab label='단계별 주소 검색' />
            </Tabs>
          </AppBar>
          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={tabValue}
            onChangeIndex={this.handleChangeIndex}>
            <TabContainer dir={theme.direction}>
              <GeocodeSearch title="주소필터는 상세주소를 제외하고 적용됩니다. 예를 들어, '우면동 17' 입력시, '서울시 서초구 우면동' 까지 필터가 적용됩니다." poi={null} isSearch={true} />
            </TabContainer>
            <TabContainer dir={theme.direction}>
              <FormGroup row={true} className={classes.formGroup}>
                <List id='address-siDo' className={classes.addressList}>
                  <ListSubheader
                    className={classes.item}
                    key='all'
                    button={true}
                    selected={this.isSelected('siDo', 'all')}
                    onClick={event => this.handleClick(event, 'siDo', '', 'all', 'all')}>
                    <Typography variant='body2'>시도 전체</Typography>
                  </ListSubheader>
                  {searchResultStore.getStep.first
                                    && <CircularProgress className={classes.progress} />
                                }
                  {searchResultStore.addressList.siDo.map(item => (
                    <ListItem
                      className={classes.item}
                      key={item.code}
                      button={true}
                      selected={this.isSelected('siDo', item.code)}
                      onClick={event => this.handleClick(event, 'siDo', 'siGunGu', item.name, item.code)}>
                      <Typography variant='body2'>{item.name}</Typography>
                    </ListItem>
                  ))}
                </List>
                <List id='address-siGunGu' className={classes.addressList}>
                  <ListSubheader
                    className={classes.item}
                    key='all'
                    button={true}
                    selected={this.isSelected('siGunGu', 'all')}
                    onClick={event => this.handleClick(event, 'siGunGu', '', 'all', 'all')}>
                    <Typography variant='body2'>시군구 전체</Typography>
                  </ListSubheader>
                  {searchResultStore.getStep.second
                    ? <CircularProgress className={classes.progress} />
                    : searchResultStore.addressList.siGunGu.length === 0
                                            && (
                                            <div className={classes.notPrevSelected}>
                                              선택된 시도가
                                              {' '}
                                              <br />
                                              없습니다.
                                            </div>
                                            )
                                }
                  {searchResultStore.addressList.siGunGu.map(item => (
                    <ListItem
                      className={classes.item}
                      key={item.code}
                      button={true}
                      selected={this.isSelected('siGunGu', item.code)}
                      onClick={event => this.handleClick(event, 'siGunGu', 'eupMyeonDong', item.name, item.code)}>
                      <ListItemText><Typography variant='body2'>{item.name}</Typography></ListItemText>
                    </ListItem>
                  ))}
                </List>
                <List id='address-eupMyeonDong' className={classes.addressList}>
                  <ListSubheader
                    className={classes.item}
                    key='all'
                    button={true}
                    selected={this.isSelected('eupMyeonDong', 'all')}
                    onClick={event => this.handleClick(event, 'eupMyeonDong', '', 'all', 'all')}>
                    <ListItemText><Typography variant='body2'>읍면동 전체</Typography></ListItemText>
                  </ListSubheader>
                  {searchResultStore.getStep.third
                    ? <CircularProgress className={classes.progress} />
                    : searchResultStore.addressList.eupMyeonDong.length === 0
                                        && (
                                        <div className={classes.notPrevSelected}>
                                          선택된 시군구가
                                          {' '}
                                          <br />
                                          없습니다.
                                        </div>
                                        )
                                }
                  {searchResultStore.addressList.eupMyeonDong.map(item => (
                    <ListItem
                      className={classes.item}
                      key={item.code}
                      button={true}
                      selected={this.isSelected('eupMyeonDong', item.code)}
                      onClick={event => this.handleClick(event, 'eupMyeonDong', '', item.name, item.code)}>
                      <ListItemText><Typography variant='body2'>{item.name}</Typography></ListItemText>
                    </ListItem>
                  ))}
                </List>
              </FormGroup>
              <div className={classes.buttonGroup}>
                <Button
                  fullWidth={true}
                  variant='contained'
                  className={classes.cancleBtn}
                  onClick={event => this.handleResetClick(event)}>
                  삭제
                </Button>
                <Button
                  fullWidth={true}
                  variant='contained' color='primary'
                  className={classes.submitBtn}
                  onClick={event => this.handleSubmitClick(event)}>
                  적용
                </Button>
              </div>
            </TabContainer>
          </SwipeableViews>
        </div>
      )
    }
}

AddressList.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(AddressList)
