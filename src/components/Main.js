import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import CssBaseline from '@material-ui/core/CssBaseline'
import Header from './Header'
import Login from './Login'
import Search from './Search'

const styles = () => ({
  login: {
    textAlign: 'center',
  },
})

@inject('managementStore')
@observer
class Main extends Component {
  render() {
    const { classes, managementStore } = this.props
    return (
      <React.Fragment>
        <CssBaseline />
        <Header />
        {managementStore.loginStatus ? <Search /> : <Login className={classes.login} />}
      </React.Fragment>
    )
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  managementStore: PropTypes.object,
}

export default withRouter(withStyles(styles)(Main))
