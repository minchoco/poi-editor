import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Header from './Header'
import SuccessRate from './charts/SuccessRate'

const styles = () => ({
  chart: {
    float: 'center',
  },
})

@inject('managementStore')
@observer
class Statistics extends Component {
  render() {
    const { classes } = this.props
    return (
      <React.Fragment>
        <CssBaseline />
        <Header isStatistics={true} />
        <div className={classes.chart}>
          <SuccessRate />
        </div>
      </React.Fragment>
    )
  }
}

Statistics.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withRouter(withStyles(styles)(Statistics))
