import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import {
  List, ListItem, Button, Typography, TextField, Tooltip, CircularProgress,
} from '@material-ui/core'

const styles = () => ({
  textField: {
    paddingRight: 10,
    width: '50%',
  },
  buttonGroup: {
    padding: '10px',
    maxHeight: 50,
    botton: 0,
  },
  item: {
    paddingTop: '0px',
    backgroundColor: 'white',
  },
  cancleBtn: {
    width: '50%',
  },
  progress: {
    margin: '20px',
  },
  resultList: {
    maxHeight: '200px',
    overflowY: 'auto',
  },
  searchBtn: {
    marginTop: '25px',
  },
})

@inject('searchStore', 'searchResultStore')
@observer
class GeocodeSearch extends Component {
    state={
      selectedIndex: 0,
      keyword: '',
    }

    componentDidMount() {
      const { searchResultStore } = this.props
      searchResultStore.result.geocodeList = []
    }

    handleChange = (event) => {
      this.setState({ keyword: event.target.value })
    }

    handleGeocodeClick = (event) => {
      const { keyword } = this.state
      const { searchStore } = this.props

      if (event.target.value === undefined) {
        searchStore.geocode(keyword)
      } else {
        searchStore.geocode(event.target.value)
      }
    }

    handleGeocodeSelectClick = (event, index, address, isSearch) => {
      const { searchStore, searchResultStore } = this.props

      if (isSearch) {
        searchStore.setOptions('address.code', address.getCode())
        searchResultStore.addressPath = address.getShortAddress()
        this.handleSubmitClick()
      } else {
        this.setState({ selectedIndex: index })
        searchResultStore.poi.newAddress = address
      }
    }

    handleResetClick = () => {
      const { searchStore, searchResultStore } = this.props
      searchResultStore.resetSelectedAddress()
      searchStore.setOptions('address.code', undefined)
      searchStore.search(() => {
        searchResultStore.anchorEl.address = null
      })
    }

    handleSubmitClick = () => {
      const { searchStore, searchResultStore } = this.props
      searchStore.search(() => {
        searchResultStore.anchorEl.address = null
      })
    }

    isSelected(index) {
      const { selectedIndex } = this.state
      return selectedIndex === index
    }

    render() {
      const {
        classes, searchResultStore, title, isSearch,
      } = this.props
      const { keyword } = this.state

      return (
        <React.Fragment>
          <Tooltip title={title}>
            <TextField
              id='address-search'
              name='terms'
              className={classes.textField}
              margin='normal'
              value={keyword}
              label='주소를 입력하세요.'
              onChange={e => this.handleChange(e)}
              onKeyPress={(e) => { if (e.key === 'Enter') this.handleGeocodeClick(e) }} />
          </Tooltip>
          <Button
            className={classes.searchBtn}
            variant='contained'
            onClick={e => this.handleGeocodeClick(e)}>
          검색
          </Button>
          {searchResultStore.geocoding
                    && <CircularProgress className={classes.progress} />
                }
          {searchResultStore.result.geocodeList.length > 0 && isSearch
            ? <Typography variant='caption'>아래 주소 중 원하는 주소를 클릭하세요.</Typography>
            : searchResultStore.result.geocodeList.length > 0 && <Typography variant='caption'>아래 주소 중 원하는 주소를 클릭한 후, 아래 주소 등록 버튼을 누르세요.</Typography> }
          <List className={classes.resultList}>
            {searchResultStore.result.geocodeList.map((address, index) => (
              <ListItem
                className={classes.item}
                key={address.id}
                selected={this.isSelected(index)}
                onClick={e => this.handleGeocodeSelectClick(e, index, address, isSearch)}
                button={true}>
                <Typography variant='body2'>
                  {address.getAddress()}
                </Typography>
              </ListItem>
            ))}
          </List>
          { isSearch
                    && (
                    <div className={classes.buttonGroup}>
                      <Button
                        fullWidth={true}
                        variant='contained'
                        className={classes.cancleBtn}
                        onClick={event => this.handleResetClick(event)}>
                            삭제
                      </Button>
                    </div>
                    ) }
        </React.Fragment>
      )
    }
}

GeocodeSearch.propTypes = {
  classes: PropTypes.object.isRequired,
  isSearch: PropTypes.bool,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
  title: PropTypes.string,
}

export default withStyles(styles, { withTheme: true })(GeocodeSearch)
