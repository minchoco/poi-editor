import React from 'react'
import PropTypes from 'prop-types'
import Autosuggest from 'react-autosuggest'
import { observer, inject } from 'mobx-react'
import { withStyles } from '@material-ui/core/styles'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import {
  TextField, Paper, MenuItem, Select, InputBase, Button,
} from '@material-ui/core'

function renderInputComponent(inputProps) {
  const {
    classes, inputRef = () => {}, ref, ...other
  } = inputProps

  return (
    <TextField
      InputProps={{
        inputRef: (node) => {
          ref(node)
          inputRef(node)
        },
        classes: {
          input: classes.input,
        },
      }}
      {...other} />
  )
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.terms, query)
  const parts = parse(suggestion.terms, matches)

  return (
    <MenuItem selected={isHighlighted} component='div' style={{ height: '10px', fontSize: '0.9rem' }}>
      <div>
        {parts.map((part, index) => (part.highlight ? (
          <span key={String(index)} style={{ fontWeight: 500 }}>
            {part.text}
          </span>
        ) : (
          <strong key={String(index)} style={{ fontWeight: 300 }}>
            {part.text}
          </strong>
        )),)}
      </div>
    </MenuItem>
  )
}

function getSuggestionValue(suggestion) {
  return suggestion.terms
}

const BootstrapInput = withStyles(theme => ({
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: 'auto',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
  },
}))(InputBase)

const styles = theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
  },
  container: {
    position: 'relative',
  },
  input: {
    padding: '10px',
    minWidth: '400px',
  },
  buttonGroup: {
    padding: '10px',
    display: 'flex',
    width: '500px',
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
  suggestResultBox: {
    marginLeft: '10px',
    position: 'absolute',
    zIndex: 10,
  },
  outlinedButtom: {
    textTransform: 'uppercase',
    margin: theme.spacing.unit,
  },
})

@inject('searchStore', 'managementStore', 'searchResultStore')
@observer
class Autocomplete extends React.Component {
  state = {
    single: '',
    suggestions: [],
  };

  handleSuggestionsFetchRequested = () => {
    const { searchResultStore } = this.props
    this.setState({
      suggestions: searchResultStore.result.suggestList,
    })
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    })
  };

  handleChange = name => (event, { newValue }) => {
    const { searchStore, searchResultStore } = this.props

    if (name !== 'mode') {
      this.handleSuggestionsClearRequested()
      searchStore.setTerms(newValue)
      searchStore.autocomplete()
      this.setState({
        [name]: newValue,
      })
    } else {
      const { value } = event.target
      const { single } = this.state
      searchResultStore.searchMode = value
      searchStore.setTerms(searchStore.terms)
      if (single !== '') searchStore.search()
    }
  };

  handleKeyPress = (e) => {
    const { searchStore } = this.props
    if (e.key === 'Enter') searchStore.search()
  };

  handleClick = () => {
    const { searchStore } = this.props
    searchStore.search()
  };

  render() {
    const { classes, searchResultStore } = this.props
    const { suggestions, single } = this.state

    const autosuggestProps = {
      renderInputComponent,
      suggestions,
      onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
      getSuggestionValue,
      renderSuggestion,
    }

    return (
      <div className={classes.root}>
        <Select
          value={searchResultStore.searchMode}
          onChange={this.handleChange('mode')}
          input={<BootstrapInput name='mode' />}>
          <MenuItem value='integrated'>통합검색</MenuItem>
          <MenuItem value='data'>데이터검색</MenuItem>
        </Select>
        <Autosuggest
          {...autosuggestProps}
          inputProps={{
            classes,
            className: classes.input,
            placeholder: 'Search KT Maps',
            value: single,
            onChange: this.handleChange('single'),
            onKeyPress: this.handleKeyPress,
            variant: 'outlined',
          }}
          theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
          }}
          renderSuggestionsContainer={options => (
            <Paper {...options.containerProps} square={true} className={classes.suggestResultBox}>
              {options.children}
            </Paper>
          )} />
        <Button variant='outlined' className={classes.outlinedButtom} onClick={this.handleClick}>
          검색
        </Button>
      </div>
    )
  }
}

Autocomplete.propTypes = {
  classes: PropTypes.object.isRequired,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(Autocomplete)
