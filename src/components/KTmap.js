import React, { Component } from 'react'
import { observer, inject } from 'mobx-react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// import Card from '@material-ui/core/Card'
// import CardContent from '@material-ui/core/CardContent'
// import CardActionArea from '@material-ui/core/CardActionArea'
// import Typography from '@material-ui/core/Typography'
import PoiResult from '../models/PoiResult'

const styles = {
  map: {
    height: '100%',
    textAlign: 'center',
  },
  card: {
    minWidth: 300,
    position: 'absolute',
    zIndex: 1,
    bottom: 10,
    display: 'inline-block',
  },
}

@inject('searchStore', 'mapElementStore', 'searchResultStore')
@observer
class KTmap extends Component {
    state= {
      // rgeocode: false,
    }

    componentDidMount() {
      const { lat, lng, mapElementStore } = this.props
      const map = new window.olleh.maps.Map(this.map, {
        center: new window.olleh.maps.LatLng(lat, lng),
        zoom: 8,
        panControl: false,
        mapTypeControl: false,
      })
      mapElementStore.setMap(map)
    }

    componentDidUpdate() {
      const { searchResultStore, mapElementStore } = this.props
      if (searchResultStore.resultMode === 'poi' && mapElementStore.resultPoiMarkerList.length > 0) {
        mapElementStore.handleResultList(mapElementStore.resultPoiMarkerList, 'show')
        mapElementStore.handleResultList(mapElementStore.resultAddressMarkerList, 'hide')
        mapElementStore.handleResultList(mapElementStore.resultAddressPolygonList, 'hide')
        mapElementStore.fitMap('marker', mapElementStore.resultPoiMarkerList)
      } else if (searchResultStore.resultMode === 'address' && mapElementStore.resultAddressMarkerList.length > 0) {
        mapElementStore.handleResultList(mapElementStore.resultPoiMarkerList, 'hide')
        mapElementStore.handleResultList(mapElementStore.resultAddressMarkerList, 'show')
        mapElementStore.handleResultList(mapElementStore.resultAddressPolygonList, 'show')
        mapElementStore.fitMap('polygon', mapElementStore.resultAddressPolygonList)
      }
    }

    handleMousePress = () => {
      // if (this.state.rgeocode) {
      //   this.setState(({ rgeocode: false }))
      // } else {
      //   this.buttonPressTimer = setTimeout(() => {
      //     this.setState(({ rgeocode: true }))
      //   }
      // }
    }

    // handleMouseRelease = () => {
    //     clearTimeout(this.buttonPressTimer);
    // }

    handleMarkerClick = (event, id) => {
      const { searchStore, mapElementStore } = this.props
      searchStore.get(id).then((response) => {
        if (response.status === 200) {
          const result = new PoiResult(response.data.pois[0])
          mapElementStore.selectedId = id
          mapElementStore.setWindow(event, '', result)
        }
      })
    }

    render() {
      const { classes, mapElementStore, searchResultStore } = this.props
      // const { rgeocode } = this.state

      return (
        <div
          id='map' ref={(c) => { this.map = c }} className={classes.map}>
          {searchResultStore.resultMode === 'poi' && mapElementStore.resultPoiMarkerList.forEach((item) => {
            item.overlay.onEvent('click', event => this.handleMarkerClick(event, item.id))
          })}

          {/* { rgeocode
                    && (
                    <Card className={classes.card}>
                      <CardActionArea>
                        <CardContent>
                          <Typography variant='h5' component='h2'>
                                    KT연구개발센터
                            {' '}
                            <span style={{ fontSize: 15 }}>연구소</span>
                          </Typography>
                          <Typography color='textSecondary'>
                                서울시 서초구 태봉로 151 (우면동 17)
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                    )
                } */}
        </div>
      )
    }
}

KTmap.propTypes = {
  classes: PropTypes.object,
  lat: PropTypes.string,
  lng: PropTypes.string,
  mapElementStore: PropTypes.object,
  searchResultStore: PropTypes.object,
  searchStore: PropTypes.object,
}

export default withStyles(styles)(KTmap)
