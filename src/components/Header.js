import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { observer, inject } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  Typography, Toolbar, AppBar, Button, Modal,
} from '@material-ui/core'
import { AccountCircle, Computer } from '@material-ui/icons'
import Autocomplete from './common/Autocomplete'
import SearchOption from './SearchOption'
import CategoryList from './lists/CategoryList'
import AddressList from './lists/AddressList'
import ResultList from './lists/ResultList'
import FilterList from './lists/FilterList'
import Editor from './modals/Editor'
import PoiResult from '../models/PoiResult'
import ResultAnalyzer from './lists/ResultAnalyzer'

const logo = require('../images/kt_logo.png')

const styles = theme => ({
  toolBar: {
    minHeight: 60,
  },
  options: {
    borderTop: '1px solid rgb(235, 235, 235)',
    minHeight: 50,
  },
  menus: {
    paddingLeft: '20px',
  },
  appBar: {
    position: 'sticky',
    boxShadow: 'none',
    borderBottom: `1px solid ${theme.palette.grey['100']}`,
    backgroundColor: 'white',
    dispaly: 'flex',
  },
  inline: {
    display: 'inline',
    paddingRight: '10px',
  },
  loginButton: {
    float: 'right',
  },
  flex: {
    display: 'flex',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
  },
  flexWithStatistics: {
    display: 'block',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      justifyContent: 'space-evenly',
      alignItems: 'center',
    },
  },
  loginStatus: {
    right: 10,
    float: 'right',
    display: 'flex',
  },
  loginIcon: {
    marginTop: 13,
    marginLeft: 10,
  },
  loginMessage: {
    textTransform: 'uppercase',
    marginTop: 15,
  },
  outlinedButtom: {
    textTransform: 'uppercase',
    margin: theme.spacing.unit,
  },
  result: {
    right: 0,
  },
  link: {
    textDecoration: 'none',
  },
})

@inject('managementStore', 'searchResultStore')
@observer
class Header extends Component {
  state = {
    modalOpen: false,
  };

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  handleLogout = () => {
    const { managementStore, searchResultStore } = this.props
    managementStore.clearStore()
    searchResultStore.clearStore()
  }

  handleModalOpen = () => {
    const { searchResultStore } = this.props
    searchResultStore.editModalOpen = true
    this.setState({ modalOpen: true })
  }

  handleModalClose = () => {
    const { searchResultStore } = this.props
    searchResultStore.editModalOpen = false
    this.setState({ modalOpen: false })
  }

  render() {
    const {
      classes, managementStore, searchResultStore, isStatistics,
    } = this.props
    const { modalOpen } = this.state

    const afterLogin = (
      <div className={!isStatistics ? classes.flex : classes.flexWithStatistics}>
        {!isStatistics
        && <Autocomplete />}
        <div className={classes.loginStatus}>
          {managementStore.admin
          && (
          <Button
            component={Link} to={!isStatistics ? '/tools/statistics' : '/tools'} variant='outlined'
            className={classes.outlinedButtom}>
            {!isStatistics ? '통계' : '검색화면으로 돌아가기'}
          </Button>
          )
          }
          <Computer className={classes.loginIcon} />
          <Typography className={classes.loginMessage}>
            {managementStore.serverZone}
          </Typography>
          {managementStore.environment !== '' && (
            <Typography className={classes.loginMessage}>
              (
              {managementStore.environment}
              )
            </Typography>
          )}
          <AccountCircle className={classes.loginIcon} />
          <Typography className={classes.loginMessage}>
            {managementStore.nickName}
          </Typography>
          <Button variant='outlined' className={classes.outlinedButtom} onClick={this.handleLogout}>Logout</Button>
        </div>
      </div>
    )

    const filters = (
      <Toolbar className={classes.options}>
        <SearchOption type='filter' contents={<FilterList />} defaultTitle='기본 필터' />
        <SearchOption type='category' contents={<CategoryList isSearch={true} />} defaultTitle='카테고리 필터' />
        <SearchOption type='address' contents={<AddressList />} defaultTitle='주소 필터' />
        { (searchResultStore.searchMode === 'integrated' && searchResultStore.numberOfPois === 0 && searchResultStore.numberOfAddress === 0)
        || searchResultStore.searchMode === 'data'
          ? null : (
            <SearchOption type='result' contents={<ResultList />} className={classes.result} />
          )
        }
        {searchResultStore.searchOnce && (
        <SearchOption
          type='analyzer' contents={<ResultAnalyzer />} defaultTitle='검색 결과 분석'
          className={classes.result} />
        )}
        { (managementStore.admin || managementStore.editor)
        && (
        <div className={classes.loginStatus} style={{ position: 'absolute' }}>
          <Button
            variant='outlined' className={classes.outlinedButtom} onClick={this.handleModalOpen}>
            POI 신규 등록
          </Button>
          <Button variant='outlined' className={classes.outlinedButtom} disabled={true}>POI 변경사항 관리</Button>
        </div>
        )}
      </Toolbar>
    )

    return (
      <AppBar position='absolute' color='default' className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          <div className={classes.inline}>
            <Typography variant='subtitle1' color='inherit' noWrap={true}>
              <Link to='/tools' className={classes.link}>
                <img width={20} src={logo} alt='logo' />
              </Link>
            </Typography>
          </div>
          {managementStore.loginStatus && afterLogin}
        </Toolbar>
        {!isStatistics && managementStore.loginStatus && filters}
        <Modal
          aria-labelledby='simple-modal-title'
          aria-describedby='simple-modal-description'
          open={modalOpen && searchResultStore.editModalOpen}
          onClose={this.handleModalClose}>
          <Editor poi={new PoiResult()} type='I' />
        </Modal>
      </AppBar>
    )
  }
}

Header.propTypes = {
  classes: PropTypes.object,
  isStatistics: PropTypes.bool,
  managementStore: PropTypes.object,
  searchResultStore: PropTypes.object,
}

export default withRouter(withStyles(styles)(Header))
