import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import KTmap from './KTmap'
import SearchResultList from './lists/SearchResultList'

const styles = () => ({
  root: {
    flexGrow: 1,
    width: '100%',
    margin: 0,
  },
  map: {
    position: 'fixed',
    right: 0,
    width: '100%',
    height: '88%',
  },
  searhResultList: {
    zIndex: 1,
  },
})

@inject('managementStore')
@observer
class Search extends Component {
  render() {
    const { classes } = this.props
    return (
      <Grid container={true} className={classes.root} alignItems='stretch'>
        <Grid
          item={true} xs={12} md={5}
          className={classes.searhResultList}>
          <SearchResultList />
        </Grid>
        <Grid
          item={true} xs={12} md={7}
          className={classes.map}>
          <KTmap lat={37.57201137787062} lng={126.97888043751267} />
        </Grid>
      </Grid>
    )
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withRouter(withStyles(styles)(Search))
