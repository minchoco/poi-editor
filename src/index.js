/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react'; 
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ManagementStore from './stores/ManagementStore'; 
import MapElementStore from './stores/MapElementStore';
import SearchResultStore from './stores/SearchResultStore';
import SearchStore from './stores/SearchStore'; 
import ErrorBoundary from './ErrorBoundary';

const managementStore = new ManagementStore(); 
const mapElementStore = new MapElementStore();
const searchResultStore = new SearchResultStore();

const searchStore = new SearchStore(managementStore, mapElementStore, searchResultStore);

ReactDOM.render(<Provider managementStore={managementStore} mapElementStore={mapElementStore} searchResultStore ={searchResultStore} searchStore={searchStore}>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
