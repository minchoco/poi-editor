/* eslint-disable */
import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import './App.css';
import Routes from './routes'

const theme = createMuiTheme({
  typography: { 
    fontFamily: [
      '"Lato"',
      'sans-serif'
    ].join(','),
    useNextVariants: true,
 },
});


class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Routes />
      </MuiThemeProvider>
    );
  }
}

export default App;
