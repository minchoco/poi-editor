import React from 'react'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Main from './components/Main'
import Statistics from './components/Statistics'
import ScrollToTop from './utils/ScrollTop'

export default () => (
  <Router>
    <ScrollToTop>
      <Route exact={true} path='/' render={() => <Redirect to='/tools' />} />
      <Route exact={true} path='/tools' component={Main} />
      <Route exact={true} path='/tools/statistics' component={Statistics} />
    </ScrollToTop>
  </Router>
)
